﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace _05文档对象模型
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Student> list = new List<Student>();
            list.Add(new Student() { ID=1,Name="大哥",Gender='男',Age=30});
            list.Add(new Student() { ID=2,Name="二弟",Gender='男',Age=25});
            list.Add(new Student() { ID=3,Name="三弟",Gender='男',Age=23});
            list.Add(new Student() { ID=4,Name="赵龙",Gender='男',Age=18});
            list.Add(new Student() { ID=5,Name="黄忠",Gender='男',Age=48});

            XmlDocument doc = new XmlDocument();
            XmlDeclaration dec = doc.CreateXmlDeclaration("1.0","utf-8",null);
            doc.AppendChild(dec);


            XmlElement person = doc.CreateElement("Person");
            doc.AppendChild(person);

            for (int i=0;i<list.Count;i++)
            {
                XmlElement student = doc.CreateElement("Student");
                student.SetAttribute("studentID",list[i].ID.ToString());

                XmlElement name = doc.CreateElement("Name");
                name.InnerXml = list[i].Name;

                XmlElement age = doc.CreateElement("Age");
                age.InnerXml = list[i].Age.ToString();

                XmlElement gender = doc.CreateElement("Gender");
                gender.InnerXml=list[i].Gender.ToString();

                person.AppendChild(student);
                student.AppendChild(name);
                student.AppendChild(age);
                student.AppendChild(gender);
            }

            doc.Save("Student.xml");
            Console.WriteLine("保存成功");
            Console.ReadKey();
        }
    }

    class Student
    {
        public int Age { get; set; }
        public string Name { get; set; }
        public int ID { get; set; }
        public char Gender { get; set; }
    }
}
