﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _08判断是否登陆成功
{
    #region << 版 本 注 释 >>
    /*
     * ========================================================================
     * Copyright  2020-2021 个人开发  .
     * ========================================================================
     * 文件名：  Student
     * 版本号：  V1.0.0.0
     * 创建人：  xieguocheng
     * 创建时间： 2021-07-26 15:57:08
     * 描述    :
     * =====================================================================
     * 修改时间：2021-07-26 15:57:08
     * 修改人  ： xieguocheng
     * 版本号  ： V1.0.0.0
     * 描述    ：
     */
    #endregion
    public class Student
    {
        public int ID
        {
            get;set;
        }
        public string Name
        {
            get;set;
        }
        public char Gender
        {
            get;set;
        }
        public int Age
        {
            get;set;
        }
        public override bool Equals(object obj)
        {
            Student s = obj as Student;
            if (this.Name==s.Name&&this.Age==s.Age&&this.Gender==s.Gender&&this.ID==s.ID)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
