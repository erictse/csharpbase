﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace _08判断是否登陆成功
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        List<Student> list = new List<Student>();
        private void Form1_Load(object sender, EventArgs e)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load("Person.xml");
            XmlElement person = doc.DocumentElement;
            XmlNodeList xnl = person.ChildNodes;
            foreach (XmlNode item in xnl)
            {
                int id = Convert.ToInt32(item.Attributes["studentID"].Value);
                string name = item["Name"].InnerText;
                int age = Convert.ToInt32(item["Age"].InnerText);
                char gender = Convert.ToChar(item["Gender"].InnerText);

                Student student = new Student();
                student.Name = name;
                student.Age = age;
                student.ID = id;
                student.Gender = gender;

                list.Add(student);
            }
        }
    }
}
