﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace _06对xml文档实现增删改查
{
    class Program
    {
        static void Main(string[] args)
        {
            #region 对xml文档实现追加的需求
            //XmlDocument doc = new XmlDocument();
            //if (File.Exists("Student.xml"))
            //{
            //    //加载文件
            //    doc.Load("Student.xml");
            //    //追加
            //    //获得跟节点跟根节点添加子节点
            //    XmlElement person = doc.DocumentElement;

            //    XmlElement student = doc.CreateElement("Student");
            //    student.SetAttribute("studentID", "10");
            //    person.AppendChild(student);

            //    XmlElement name = doc.CreateElement("Name");
            //    name.InnerXml = "我是新来的";
            //    student.AppendChild(name);

            //    XmlElement age = doc.CreateElement("Age");
            //    age.InnerXml = "18";
            //    student.AppendChild(age);
            //    XmlElement gender = doc.CreateElement("Gender");
            //    gender.InnerXml = "女";
            //    student.AppendChild(gender);

            //}
            //else
            //{
            //    Console.WriteLine("测试先");
            //}

            //doc.Save("Student.xml"); 
            #endregion

            #region 修改属性
            //XmlDocument doc = new XmlDocument();
            //if (File.Exists("Student.xml"))
            //{
            //    //加载文件
            //    doc.Load("Student.xml");

            //    //获得根节点
            //    XmlElement students = doc.DocumentElement;
            //    //获取根节点下面的所有子节点
            //    XmlNodeList xnl = students.ChildNodes;
            //    foreach (XmlNode item in xnl)
            //    {

            //        Console.WriteLine(item.Attributes["studentID"].Value);
            //    }

            //    //XmlElement student = students["Student"];
            //}
            #endregion


            XmlDocument doc = new XmlDocument();
            doc.Load("Student.xml");
            //获取根节点
            XmlElement students = doc.DocumentElement;

            XmlNode xn = students.SelectSingleNode("/Person/Student[@studentID='1']");
            Console.WriteLine(xn.InnerText);

            Console.WriteLine("程序结束");
                Console.ReadKey();
        }
    }
}
