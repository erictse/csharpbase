﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _04单例设计模式
{
    public partial class Form2 : Form
    {
        private Form2()
        {
            InitializeComponent();
        }
        private static Form2 _single = null;
        public static Form2 GetSingle()
        {
            if (_single==null)
            {
                _single = new Form2();
            }
            return _single;
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            _single = new Form2();
        }
    }
}
