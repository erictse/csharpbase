﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _03TreeView的使用
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            treeView1.Nodes.Add(txtParent.Text.Trim());
        }

        private void button2_Click(object sender, EventArgs e)
        {
            TreeNode tn = treeView1.SelectedNode;
            tn.Nodes.Add(txtChild.Text);
        }
    }
}
