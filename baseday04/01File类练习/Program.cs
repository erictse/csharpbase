﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01File类练习
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = @"D:\VS\base\csharpbase\工资.txt";
            string[] contents = File.ReadAllLines(path,Encoding.Default);
            
            for (int i=0;i<contents.Length;i++)
            {
                string[] temp = contents[i].Split(new char[] { ' '},StringSplitOptions.RemoveEmptyEntries);
                Console.WriteLine(contents[i]);
                contents[i] = temp[0] + "   " + int.Parse(temp[1]) * 2;
                Console.WriteLine(contents[i]);
            }

            File.WriteAllLines(path,contents);
            

            Console.ReadKey();
        }
    }
}
