﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02Directory
{
    class Program
    {
        static void Main(string[] args)
        {
            //CreateDirectory:创建一个新的文件夹
            //Delete:删除
            //Move：剪切
            //Exist()判断指定的文件夹是否存在
            if (!Directory.Exists(@"D:\VS\base\csharpbase\B2"))
            {
                Directory.CreateDirectory(@"D:\VS\base\csharpbase\B2");
            }
        }
    }
}
