﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _04资料管理器
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string path = @"D:\VS2017\demo";
            LoadDirectoryAndFile(path,treeView1.Nodes);
            
        }
        private void LoadDirectoryAndFile(string path,TreeNodeCollection tc)
        {
            //获取当前这一目录下所有文件夹的路径
            string[] dics = Directory.GetDirectories(path);
            for (int i=0;i<dics.Length;i++)
            {
                string dicName = Path.GetFileName(dics[i]);//Path.GetFileNameWithoutExtension(dics[i]);
                TreeNode tn = tc.Add(dicName);
                LoadDirectoryAndFile(dics[i],tn.Nodes);
            }

            string[] fileNames = Directory.GetFiles(path);
            for (int i =0;i<fileNames.Length;i++)
            {
                TreeNode tn = tc.Add(Path.GetFileName(fileNames[i]));
                tn.Tag = fileNames[i];
            }
        }

        private void treeView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            //获取选中的节点
            string filePath = treeView1.SelectedNode.Tag.ToString();
            textBox1.Text = File.ReadAllText(filePath,Encoding.UTF8);
        }
    }
}
