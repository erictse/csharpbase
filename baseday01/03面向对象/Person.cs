﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _03面向对象
{
    #region << 版 本 注 释 >>
    /*
     * ========================================================================
     * Copyright  2020-2021 个人开发  .
     * ========================================================================
     * 文件名：  Person
     * 版本号：  V1.0.0.0
     * 创建人：  xieguocheng
     * 创建时间： 2021-07-20 16:40:19
     * 描述    :
     * =====================================================================
     * 修改时间：2021-07-20 16:40:19
     * 修改人  ： xieguocheng
     * 版本号  ： V1.0.0.0
     * 描述    ：
     */
    #endregion
    public class Person
    {
        //字段、属性、函数、构造函数...
        //字段：存储数据
        //属性：保护字段 get set
        //函数：描述对象的行为
        //构造函数：初始化对象，给对象的每个属性进行赋值
        string _name;
        public string Name
        {
            get { return _name; }
            set
            {
                if(value !="张三")
                {
                    value = "张三";
                }
                _name = value;
            }
        }

        int _age;
        public int Age
        {
            get
            {
                if (_age < 0 || _age > 100)
                {
                    return _age = 18;
                }
                return _age;
            }
            set
            {
                _age = value;
            }
        }
        public char Gender { get; set; }

        public Person(char gender)
        {
            if(gender !='男' && gender != '女')
            {
                gender = '男';
            }
            this.Gender = gender;
        }
    }
}
