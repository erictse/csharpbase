﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04三个关键字
{
    class Student:Person
    {
        public new void SayHello()
        {
            Console.WriteLine("我是学生类子类");
        }

        public Student GetStudent()
        {
            return this;
        }

        public void GetPerson()
        {
            base.SayHello();
        }

        public string Name { get; set; }

        public void Test()
        {
            string Name = "春生老师";
            Console.WriteLine("我的名字{0}",this.Name);
        }
    }

   
}
