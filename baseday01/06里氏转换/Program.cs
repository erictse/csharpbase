﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _06里氏转换
{
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person();
            if (person is Teacher)
            {
                ((Teacher)person).TeacherSayHello();
            }
            else
            {
                Console.WriteLine("转换失败");
            }

            //Student s = new Student();
            Student s = person as Student;

            if (s!=null)
            {
                s.StudentSayHello();
            }
            else
            {
                Console.WriteLine("转换失败");
            }

            Console.ReadKey();
        }
    }

    class Person
    {
        public void PersonSayHello()
        {
            Console.WriteLine("我是父类");
        }
    }

    class Student : Person
    {
        public void StudentSayHello()
        {
            Console.WriteLine("我是学生类");
        }
    }
    class Teacher : Person
    {
        public void TeacherSayHello()
        {
            Console.WriteLine("我是老师");
        }
    }

}
