﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01进程
{
    class Program
    {
        static void Main(string[] args)
        {
            Process[] pro = Process.GetProcesses();
            int proCont = 0;
            foreach (var item in pro)
            {
                //item.Kill();
                Console.WriteLine(item.ProcessName);
                proCont++;
            }

            //Process.Start("firefox");
            //Process.Start("mspaint");
            //Process.Start("iexplore");
            //Process.Start("calc");

            //封装我们要打开的文件 但是并不去打开这个文件
            ProcessStartInfo psi = new ProcessStartInfo(@"D:\VS2017\csharpbase\baseday01\02打开文件练习\bin\Debug\02打开文件练习.exe");
            //创建进程对象
            Process pro01 = new Process();
            //告诉进程要打开的文件信息
            pro01.StartInfo = psi;
            //调用函数打开
            pro01.Start();

            Console.WriteLine(proCont);
            Console.ReadKey();
        }
    }
}
