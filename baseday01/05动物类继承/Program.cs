﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05动物类继承
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal[] animals = { new Cat(), new Dog(), new Pig() };
            for (int i=0;i<animals.Length;i++)
            {
                animals[i].Bark();
                animals[i].Eat();
                animals[i].Drink();
            }

            Console.ReadKey();
        }
    }
}
