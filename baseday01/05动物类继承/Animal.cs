﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05动物类继承
{
    abstract class Animal
    {
        public abstract void Bark();
        public void Eat()
        {
            Console.WriteLine("动物可以舔着吃");
        }

        public void Drink()
        {
            Console.WriteLine("动物可以舔着喝");
        }
    }
}
