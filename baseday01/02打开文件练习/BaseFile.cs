﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02打开文件练习
{
    #region << 版 本 注 释 >>
    /*
     * ========================================================================
     * Copyright  2020-2021 个人开发  .
     * ========================================================================
     * 文件名：  BaseFile
     * 版本号：  V1.0.0.0
     * 创建人：  xieguocheng
     * 创建时间： 2021-07-20 14:56:10
     * 描述    :
     * =====================================================================
     * 修改时间：2021-07-20 14:56:10
     * 修改人  ： xieguocheng
     * 版本号  ： V1.0.0.0
     * 描述    ：
     */
    #endregion
    public class BaseFile
    {
        //字段、属性、构造函数、函数、索引器
        private string _filePath;
        public string FilePath//ctrl+R+E
        {
            get { return _filePath; }
            set { _filePath = value; }
        }
        //自动属性 prop+两下tab
        public string FileName { get; set; }

        public BaseFile(string filePath,string fileName)
        {
            this.FilePath = filePath;
            this.FileName = fileName;
        }

        //设计一个方法  用来打开指定的文件
        public void OpenFile()
        {
            ProcessStartInfo psi=new ProcessStartInfo(this.FilePath+"\\"+this.FileName);
            Process pro = new Process();
            pro.StartInfo = psi;
            pro.Start();
        }

        

    }
}
