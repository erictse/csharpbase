﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02打开文件练习
{
    #region << 版 本 注 释 >>
    /*
     * ========================================================================
     * Copyright  2020-2021 个人开发  .
     * ========================================================================
     * 文件名：  TxtFile
     * 版本号：  V1.0.0.0
     * 创建人：  xieguocheng
     * 创建时间： 2021-07-20 15:04:28
     * 描述    :
     * =====================================================================
     * 修改时间：2021-07-20 15:04:28
     * 修改人  ： xieguocheng
     * 版本号  ： V1.0.0.0
     * 描述    ：
     */
    #endregion
    public class TxtFile:BaseFile
    {
        public TxtFile(string filePath,string fileName)
            :base(filePath,fileName)
        {

        }
    }
}
