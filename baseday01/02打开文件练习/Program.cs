﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02打开文件练习
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入要打开的文件所在路径");
            string filePath = Console.ReadLine();
            Console.WriteLine("请输入要打开的文件名字");
            string fileName = Console.ReadLine();

            BaseFile bf = GetFile(filePath,fileName);
            if(bf != null)
            {
                bf.OpenFile();
            }
            Console.ReadKey();

        }

        static BaseFile GetFile(string filePath,string fileName)
        {
            BaseFile bf = null;
            string strExtension = Path.GetExtension(fileName);
            string[] filesExtension = { ".txt", ".avi", ".mp4", ".exe" };
            Console.WriteLine(filesExtension[0]);
            foreach (string file in filesExtension)
            {
                Console.WriteLine(file);
            }
            switch (strExtension) 
            {
                case ".txt":
                    bf = new TxtFile(filePath,fileName);
                    break;
                case ".avi":
                    bf = new AviFile(filePath,fileName);
                    break;
                case ".mp4":
                    bf = new MP4File(filePath,fileName);
                    break;
                case ".exe":
                    bf = new ExeFile(filePath,fileName);
                    break;
            }
            return bf;


        }
    }
}
