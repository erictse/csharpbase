﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProOperation;
namespace Cheng
{
    public class C:Operation
    {

        public C(int n1, int n2)
            : base(n1, n2)
        { }
        public override int GetResult()
        {
            return this.NumberOne * this.NumberTwo;
        }

        public override string Type
        {
            get { return "*"; }
        }
    }
}
