﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SerialPortComm
{
    public partial class form_SPDataAlter : Form
    {
        SerialPort sp;
        public form_SPDataAlter(SerialPort a)
        {
            form_Home.form_home.Enabled = false;
            InitializeComponent();
            sp = a;//获得一个串口对象

        }

        private void form_SPDataAlter_Load(object sender, EventArgs e)
        {

            cmb_StopBits.DataSource = new string[] { "One", "OnePointFive", "Two", "None" };
            cmb_StopBits.DropDownStyle = ComboBoxStyle.DropDownList;

            cmb_Parity.DataSource = new string[] { "Odd", "Even", "None" };
            cmb_Parity.DropDownStyle = ComboBoxStyle.DropDownList;

            this.FormClosed += form_SPDataAlter_FormClosed;//窗体关闭事件

            lbl_SPName.Text = sp.PortName;
            txt_BaudRate.Text = sp.BaudRate.ToString();
            txt_DataBits.Text = sp.DataBits.ToString();

            if (sp.StopBits ==StopBits.None)
            {
                this.cmb_StopBits.Text ="";
            }
            else
	        {
                this.cmb_StopBits.Text = sp.StopBits.ToString();
	        }

            if (sp.Parity == Parity.None)
            {
                this.cmb_Parity.Text = "";
            }
            else
	        {
                this.cmb_Parity.Text = sp.Parity.ToString();
	        }



            btn_Alter.Click += btn_Alter_Click;
        

        }

        void form_SPDataAlter_FormClosed(object sender, FormClosedEventArgs e)
        {
            string a = string.Format("修改串口：{0}，波特率：{1}，数据位：{2}，停止位：{3}，校验位：{4}",
                sp.PortName
                , sp.BaudRate
                , sp.DataBits
                , sp.StopBits
                , sp.Parity);
             
            form_Home.form_home.MessageShowADD(a);
            form_Home.form_home.Enabled = true;
        }

        void btn_Alter_Click(object sender, EventArgs e)
        {
            int i_BaudRate ;
            if (!int.TryParse(txt_BaudRate.Text,out i_BaudRate))
	        {
                MessageBox.Show("Error：波特率不正确!", "Error");
                return;
	        }
            sp.BaudRate = i_BaudRate;
         

            int i_DataBits;
            if (!int.TryParse(txt_DataBits.Text, out i_DataBits))
            {
                MessageBox.Show("Error：数据位不正确!", "Error");
                return;
            }
            sp.DataBits =i_DataBits;
           



  
          

                switch (cmb_StopBits.Text)
                {
                    case "One":
                        sp.StopBits = System.IO.Ports.StopBits.One;
                        break;
                    case "OnePointFive":
                        sp.StopBits = System.IO.Ports.StopBits.OnePointFive;
                        break;
                    case "Two":
                        sp.StopBits = System.IO.Ports.StopBits.Two;
                        break;
                    default:
                        sp.StopBits = System.IO.Ports.StopBits.None;
                        break;

                }


                switch (cmb_Parity.Text)
                {
                    case "Odd":
                        sp.Parity = Parity.Odd;
                        break;
                    case "Even":
                        sp.Parity = Parity.Even;
                        break;
                    default:
                        sp.Parity = Parity.None;
                        break;
                }

          



            form_Home.form_home.alterSerialPortData(sp);
            form_Home.HT_SerialPortDataList[sp.PortName] = sp;
          


            if (Array.IndexOf(form_Home.config.AppSettings.Settings.AllKeys, sp.PortName + "_BaudRate") == -1)
            {
                form_Home.config.AppSettings.Settings.Add(sp.PortName + "_BaudRate", sp.BaudRate.ToString());
            }
            else
            {
                form_Home.config.AppSettings.Settings[sp.PortName + "_BaudRate"].Value = sp.BaudRate.ToString ();
            }


            if (Array.IndexOf(form_Home.config.AppSettings.Settings.AllKeys, sp.PortName + "_DataBits") == -1)
            {
                form_Home.config.AppSettings.Settings.Add(sp.PortName + "_DataBits", sp.DataBits.ToString());
            }
            else
            {
                form_Home.config.AppSettings.Settings[sp.PortName + "_DataBits"].Value = sp.DataBits.ToString();
            }

            if (Array.IndexOf(form_Home.config.AppSettings.Settings.AllKeys, sp.PortName + "_StopBits") == -1)
            {
                form_Home.config.AppSettings.Settings.Add(sp.PortName + "_StopBits", sp.StopBits.ToString());
            }
            else
            {
                form_Home.config.AppSettings.Settings[sp.PortName + "_StopBits"].Value = sp.StopBits.ToString();
            }


            if (Array.IndexOf(form_Home.config.AppSettings.Settings.AllKeys, sp.PortName + "_Parity") == -1)
            {
                form_Home.config.AppSettings.Settings.Add(sp.PortName + "_Parity", sp.Parity.ToString());
            }
            else
            {
                form_Home.config.AppSettings.Settings[sp.PortName + "_Parity"].Value = sp.Parity.ToString();
            }


            form_Home.config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");

            this.Close();
        }
    }
}
