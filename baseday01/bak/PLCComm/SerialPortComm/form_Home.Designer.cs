﻿namespace SerialPortComm
{
    partial class form_Home
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_Home));
            this.tabControl_MessageConfig = new System.Windows.Forms.TabControl();
            this.tabP_View = new System.Windows.Forms.TabPage();
            this.btn_TwoScan = new System.Windows.Forms.Button();
            this.btn_OneScan = new System.Windows.Forms.Button();
            this.btn_ShelfScan = new System.Windows.Forms.Button();
            this.txt_TarySN = new System.Windows.Forms.TextBox();
            this.txt_ShelfSN = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_View_MessageShow = new System.Windows.Forms.TextBox();
            this.DGV_NewData = new System.Windows.Forms.DataGridView();
            this.btn_AtartAutoRun1 = new System.Windows.Forms.Button();
            this.tabP_MessageConfig = new System.Windows.Forms.TabPage();
            this.btn_AtartAutoRun = new System.Windows.Forms.Button();
            this.btn_ConnPLC = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_ALSNumber = new System.Windows.Forms.TextBox();
            this.axActUtlType = new AxActUtlTypeLib.AxActUtlType();
            this.CLB_SerialPortList = new System.Windows.Forms.CheckedListBox();
            this.lbl_Parity = new System.Windows.Forms.Label();
            this.lbl_StopBits = new System.Windows.Forms.Label();
            this.lbl_DataBits = new System.Windows.Forms.Label();
            this.lbl_BaudRate = new System.Windows.Forms.Label();
            this.btn_SerialPortDataAlter = new System.Windows.Forms.Button();
            this.txt_MessageShow = new System.Windows.Forms.TextBox();
            this.tabControl_MessageConfig.SuspendLayout();
            this.tabP_View.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_NewData)).BeginInit();
            this.tabP_MessageConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axActUtlType)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl_MessageConfig
            // 
            this.tabControl_MessageConfig.Controls.Add(this.tabP_View);
            this.tabControl_MessageConfig.Controls.Add(this.tabP_MessageConfig);
            this.tabControl_MessageConfig.Location = new System.Drawing.Point(12, 2);
            this.tabControl_MessageConfig.Name = "tabControl_MessageConfig";
            this.tabControl_MessageConfig.SelectedIndex = 0;
            this.tabControl_MessageConfig.Size = new System.Drawing.Size(1074, 643);
            this.tabControl_MessageConfig.TabIndex = 15;
            // 
            // tabP_View
            // 
            this.tabP_View.Controls.Add(this.btn_TwoScan);
            this.tabP_View.Controls.Add(this.btn_OneScan);
            this.tabP_View.Controls.Add(this.btn_ShelfScan);
            this.tabP_View.Controls.Add(this.txt_TarySN);
            this.tabP_View.Controls.Add(this.txt_ShelfSN);
            this.tabP_View.Controls.Add(this.label3);
            this.tabP_View.Controls.Add(this.label2);
            this.tabP_View.Controls.Add(this.txt_View_MessageShow);
            this.tabP_View.Controls.Add(this.DGV_NewData);
            this.tabP_View.Controls.Add(this.btn_AtartAutoRun1);
            this.tabP_View.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tabP_View.Location = new System.Drawing.Point(4, 22);
            this.tabP_View.Name = "tabP_View";
            this.tabP_View.Padding = new System.Windows.Forms.Padding(3);
            this.tabP_View.Size = new System.Drawing.Size(1066, 617);
            this.tabP_View.TabIndex = 0;
            this.tabP_View.Text = "自动运行界面";
            this.tabP_View.UseVisualStyleBackColor = true;
            // 
            // btn_TwoScan
            // 
            this.btn_TwoScan.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_TwoScan.Location = new System.Drawing.Point(388, 165);
            this.btn_TwoScan.Name = "btn_TwoScan";
            this.btn_TwoScan.Size = new System.Drawing.Size(188, 53);
            this.btn_TwoScan.TabIndex = 35;
            this.btn_TwoScan.Text = "第二次托盘扫码";
            this.btn_TwoScan.UseVisualStyleBackColor = true;
            // 
            // btn_OneScan
            // 
            this.btn_OneScan.Location = new System.Drawing.Point(388, 90);
            this.btn_OneScan.Name = "btn_OneScan";
            this.btn_OneScan.Size = new System.Drawing.Size(188, 53);
            this.btn_OneScan.TabIndex = 34;
            this.btn_OneScan.Text = "第一次托盘扫码";
            this.btn_OneScan.UseVisualStyleBackColor = true;
            // 
            // btn_ShelfScan
            // 
            this.btn_ShelfScan.Location = new System.Drawing.Point(388, 22);
            this.btn_ShelfScan.Name = "btn_ShelfScan";
            this.btn_ShelfScan.Size = new System.Drawing.Size(188, 53);
            this.btn_ShelfScan.TabIndex = 33;
            this.btn_ShelfScan.Text = "料架扫码";
            this.btn_ShelfScan.UseVisualStyleBackColor = true;
            // 
            // txt_TarySN
            // 
            this.txt_TarySN.Location = new System.Drawing.Point(95, 155);
            this.txt_TarySN.Name = "txt_TarySN";
            this.txt_TarySN.Size = new System.Drawing.Size(195, 29);
            this.txt_TarySN.TabIndex = 32;
            // 
            // txt_ShelfSN
            // 
            this.txt_ShelfSN.Location = new System.Drawing.Point(95, 114);
            this.txt_ShelfSN.Name = "txt_ShelfSN";
            this.txt_ShelfSN.Size = new System.Drawing.Size(195, 29);
            this.txt_ShelfSN.TabIndex = 31;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 19);
            this.label3.TabIndex = 30;
            this.label3.Text = "料架SN：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 165);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 19);
            this.label2.TabIndex = 29;
            this.label2.Text = "TarySN:";
            // 
            // txt_View_MessageShow
            // 
            this.txt_View_MessageShow.Location = new System.Drawing.Point(9, 243);
            this.txt_View_MessageShow.Multiline = true;
            this.txt_View_MessageShow.Name = "txt_View_MessageShow";
            this.txt_View_MessageShow.Size = new System.Drawing.Size(618, 368);
            this.txt_View_MessageShow.TabIndex = 28;
            // 
            // DGV_NewData
            // 
            this.DGV_NewData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_NewData.Location = new System.Drawing.Point(633, 6);
            this.DGV_NewData.Name = "DGV_NewData";
            this.DGV_NewData.RowTemplate.Height = 23;
            this.DGV_NewData.Size = new System.Drawing.Size(427, 608);
            this.DGV_NewData.TabIndex = 27;
            // 
            // btn_AtartAutoRun1
            // 
            this.btn_AtartAutoRun1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.btn_AtartAutoRun1.Location = new System.Drawing.Point(12, 22);
            this.btn_AtartAutoRun1.Name = "btn_AtartAutoRun1";
            this.btn_AtartAutoRun1.Size = new System.Drawing.Size(281, 64);
            this.btn_AtartAutoRun1.TabIndex = 26;
            this.btn_AtartAutoRun1.Text = "启动自动运行";
            this.btn_AtartAutoRun1.UseVisualStyleBackColor = true;
            // 
            // tabP_MessageConfig
            // 
            this.tabP_MessageConfig.Controls.Add(this.btn_AtartAutoRun);
            this.tabP_MessageConfig.Controls.Add(this.btn_ConnPLC);
            this.tabP_MessageConfig.Controls.Add(this.label1);
            this.tabP_MessageConfig.Controls.Add(this.txt_ALSNumber);
            this.tabP_MessageConfig.Controls.Add(this.axActUtlType);
            this.tabP_MessageConfig.Controls.Add(this.CLB_SerialPortList);
            this.tabP_MessageConfig.Controls.Add(this.lbl_Parity);
            this.tabP_MessageConfig.Controls.Add(this.lbl_StopBits);
            this.tabP_MessageConfig.Controls.Add(this.lbl_DataBits);
            this.tabP_MessageConfig.Controls.Add(this.lbl_BaudRate);
            this.tabP_MessageConfig.Controls.Add(this.btn_SerialPortDataAlter);
            this.tabP_MessageConfig.Controls.Add(this.txt_MessageShow);
            this.tabP_MessageConfig.Location = new System.Drawing.Point(4, 22);
            this.tabP_MessageConfig.Name = "tabP_MessageConfig";
            this.tabP_MessageConfig.Padding = new System.Windows.Forms.Padding(3);
            this.tabP_MessageConfig.Size = new System.Drawing.Size(1066, 617);
            this.tabP_MessageConfig.TabIndex = 1;
            this.tabP_MessageConfig.Text = "信息配置";
            this.tabP_MessageConfig.UseVisualStyleBackColor = true;
            // 
            // btn_AtartAutoRun
            // 
            this.btn_AtartAutoRun.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.btn_AtartAutoRun.Location = new System.Drawing.Point(511, 135);
            this.btn_AtartAutoRun.Name = "btn_AtartAutoRun";
            this.btn_AtartAutoRun.Size = new System.Drawing.Size(281, 64);
            this.btn_AtartAutoRun.TabIndex = 25;
            this.btn_AtartAutoRun.Text = "启动自动运行";
            this.btn_AtartAutoRun.UseVisualStyleBackColor = true;
            // 
            // btn_ConnPLC
            // 
            this.btn_ConnPLC.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.btn_ConnPLC.Location = new System.Drawing.Point(678, 66);
            this.btn_ConnPLC.Name = "btn_ConnPLC";
            this.btn_ConnPLC.Size = new System.Drawing.Size(114, 39);
            this.btn_ConnPLC.TabIndex = 24;
            this.btn_ConnPLC.Text = "连接PLC";
            this.btn_ConnPLC.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(567, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 16);
            this.label1.TabIndex = 23;
            this.label1.Text = "PLC工站号：";
            // 
            // txt_ALSNumber
            // 
            this.txt_ALSNumber.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.txt_ALSNumber.Location = new System.Drawing.Point(676, 25);
            this.txt_ALSNumber.Name = "txt_ALSNumber";
            this.txt_ALSNumber.Size = new System.Drawing.Size(116, 26);
            this.txt_ALSNumber.TabIndex = 22;
            // 
            // axActUtlType
            // 
            this.axActUtlType.Enabled = true;
            this.axActUtlType.Location = new System.Drawing.Point(511, 15);
            this.axActUtlType.Name = "axActUtlType";
            this.axActUtlType.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axActUtlType.OcxState")));
            this.axActUtlType.Size = new System.Drawing.Size(32, 32);
            this.axActUtlType.TabIndex = 21;
            // 
            // CLB_SerialPortList
            // 
            this.CLB_SerialPortList.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.CLB_SerialPortList.FormattingEnabled = true;
            this.CLB_SerialPortList.Location = new System.Drawing.Point(9, 27);
            this.CLB_SerialPortList.Name = "CLB_SerialPortList";
            this.CLB_SerialPortList.Size = new System.Drawing.Size(202, 172);
            this.CLB_SerialPortList.TabIndex = 20;
            // 
            // lbl_Parity
            // 
            this.lbl_Parity.AutoSize = true;
            this.lbl_Parity.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_Parity.Location = new System.Drawing.Point(231, 135);
            this.lbl_Parity.Name = "lbl_Parity";
            this.lbl_Parity.Size = new System.Drawing.Size(68, 16);
            this.lbl_Parity.TabIndex = 19;
            this.lbl_Parity.Text = "检查位:";
            // 
            // lbl_StopBits
            // 
            this.lbl_StopBits.AutoSize = true;
            this.lbl_StopBits.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_StopBits.Location = new System.Drawing.Point(231, 102);
            this.lbl_StopBits.Name = "lbl_StopBits";
            this.lbl_StopBits.Size = new System.Drawing.Size(68, 16);
            this.lbl_StopBits.TabIndex = 18;
            this.lbl_StopBits.Text = "停止位:";
            // 
            // lbl_DataBits
            // 
            this.lbl_DataBits.AutoSize = true;
            this.lbl_DataBits.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_DataBits.Location = new System.Drawing.Point(231, 66);
            this.lbl_DataBits.Name = "lbl_DataBits";
            this.lbl_DataBits.Size = new System.Drawing.Size(68, 16);
            this.lbl_DataBits.TabIndex = 17;
            this.lbl_DataBits.Text = "数据位:";
            // 
            // lbl_BaudRate
            // 
            this.lbl_BaudRate.AutoSize = true;
            this.lbl_BaudRate.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_BaudRate.Location = new System.Drawing.Point(231, 35);
            this.lbl_BaudRate.Name = "lbl_BaudRate";
            this.lbl_BaudRate.Size = new System.Drawing.Size(68, 16);
            this.lbl_BaudRate.TabIndex = 16;
            this.lbl_BaudRate.Text = "波特率:";
            // 
            // btn_SerialPortDataAlter
            // 
            this.btn_SerialPortDataAlter.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Bold);
            this.btn_SerialPortDataAlter.Location = new System.Drawing.Point(234, 168);
            this.btn_SerialPortDataAlter.Name = "btn_SerialPortDataAlter";
            this.btn_SerialPortDataAlter.Size = new System.Drawing.Size(134, 31);
            this.btn_SerialPortDataAlter.TabIndex = 15;
            this.btn_SerialPortDataAlter.Text = "串口参数修改";
            this.btn_SerialPortDataAlter.UseVisualStyleBackColor = true;
            // 
            // txt_MessageShow
            // 
            this.txt_MessageShow.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold);
            this.txt_MessageShow.Location = new System.Drawing.Point(9, 234);
            this.txt_MessageShow.Multiline = true;
            this.txt_MessageShow.Name = "txt_MessageShow";
            this.txt_MessageShow.Size = new System.Drawing.Size(1048, 368);
            this.txt_MessageShow.TabIndex = 14;
            // 
            // form_Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1098, 657);
            this.Controls.Add(this.tabControl_MessageConfig);
            this.Name = "form_Home";
            this.Text = "Form1";
            this.tabControl_MessageConfig.ResumeLayout(false);
            this.tabP_View.ResumeLayout(false);
            this.tabP_View.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_NewData)).EndInit();
            this.tabP_MessageConfig.ResumeLayout(false);
            this.tabP_MessageConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axActUtlType)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl_MessageConfig;
        private System.Windows.Forms.TabPage tabP_View;
        private System.Windows.Forms.TabPage tabP_MessageConfig;
        private System.Windows.Forms.Button btn_AtartAutoRun;
        private System.Windows.Forms.Button btn_ConnPLC;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_ALSNumber;
        private AxActUtlTypeLib.AxActUtlType axActUtlType;
        private System.Windows.Forms.CheckedListBox CLB_SerialPortList;
        private System.Windows.Forms.Label lbl_Parity;
        private System.Windows.Forms.Label lbl_StopBits;
        private System.Windows.Forms.Label lbl_DataBits;
        private System.Windows.Forms.Label lbl_BaudRate;
        private System.Windows.Forms.Button btn_SerialPortDataAlter;
        private System.Windows.Forms.TextBox txt_MessageShow;
        private System.Windows.Forms.Button btn_AtartAutoRun1;
        private System.Windows.Forms.DataGridView DGV_NewData;
        private System.Windows.Forms.TextBox txt_View_MessageShow;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_TarySN;
        private System.Windows.Forms.TextBox txt_ShelfSN;
        private System.Windows.Forms.Button btn_ShelfScan;
        private System.Windows.Forms.Button btn_TwoScan;
        private System.Windows.Forms.Button btn_OneScan;
    }
}

