﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace SerialPortComm
{
    public partial class form_Home : Form
    {
        


        /// <summary>
        /// PLC访问位
        /// </summary>
        string PLCLocation;
        string PLCLocation_ShelfScanValue = "";
        string PLCLocation_ShelfScanOKValue = "";
        string PLCLocation_ShelfScanNGValue = "";
        string PLCLocation_OneTrayScanValue = "";
        string PLCLocation_OneTrayScanOKValue = "";
        string PLCLocation_OneTrayScanNGValue = "";
        string PLCLocation_TwoTrayScanValue = "";
        string PLCLocation_TwoTrayScanOKValue = "";
        string PLCLocation_TwoTrayScanNGValue = "";
        string PLCLocation_TraySNScanValue = "";
        string ShelfScanSP = "";
        double ShelfScanDateInterval = -101;
        string OneTrayScanSP = "";
        double OneTrayScanDateInterval =-101;
        string TwoTrayScanSP = "";
        double TwoTrayScanDateInterval = -101;


        /// <summary>
        /// 当前窗体类对象
        /// </summary>
        public static form_Home form_home = null;
       

        /// <summary>
        /// 当前项目配置文件对象
        /// </summary>
       public  static Configuration config;

        /// <summary>
        /// 扫码枪串口对象
        /// </summary>
       public  static Hashtable HT_SerialPortDataList = new Hashtable();

        /// <summary>
        /// 公共返回值对象
        /// </summary>
        int P_Return;
        string P_Message;


        /// <summary>
        /// 托盘SN对象
        /// </summary>
        string traySN = null;

        /// <summary>
        /// 料架SN对象
        /// </summary>
        string shelfSN;

        /// <summary>
        /// 托盘内物料信息表
        /// </summary>
        DataTable DT_Data = new DataTable();


        /// <summary>
        /// 用于控制线程循环
        /// </summary>
        int CycleIndex = -1;
        int CycleIndex2 = -1;
      

        /// <summary>
        /// PCL连接状态
        /// </summary>
        public bool PLCConnState = false;



        public form_Home()
        {
            InitializeComponent();
            formLoad();
          
        }

        void btn_ShelfScan_Click(object sender, EventArgs e)
        {
            if (!getConfigData())
            {
                MessageBox.Show("请完成配置文件修改");
                return;
            }
            SetShelfScan();
        }


        /// <summary>
        /// 初始化数据与对象，对控件进行事件绑定
        /// </summary>
        private void formLoad()
        {
            form_home = this;
            config = GetConfigFile();


            txt_MessageShow.ScrollBars = ScrollBars.Both;
            txt_MessageShow.Enabled = false;

          
            SerialPortRefresh();
            if (!getConfigData())
            {
                MessageBox.Show("请完成配置文件修改");
                return;
            }
           

            DT_Data.Columns.Add(new DataColumn("Location"));
            DT_Data.Columns.Add(new DataColumn("SN", Type.GetType("System.String")));




            //连接PLC按钮
            this.btn_ConnPLC.Click += btn_ConnPLC_Click;

            //索引值更改时事件
            this.CLB_SerialPortList.SelectedIndexChanged += CLB_SerialPortList_SelectedIndexChanged;

            //
            this.CLB_SerialPortList.ItemCheck += CLB_SerialPortList_ItemCheck;

            //接口数据修改按钮点击事件
            this.btn_SerialPortDataAlter.Click += btn_SerialPortDataAlter_Click;
                

            //取消线程调用设置
            Control.CheckForIllegalCrossThreadCalls = false;


            //启动自动按钮点击事件
            this.btn_AtartAutoRun.Click += btn_AtartAutoRun_Click;
            this.btn_AtartAutoRun1.Click += btn_AtartAutoRun_Click;
            this.btn_ShelfScan.Click += btn_ShelfScan_Click;
            this.btn_OneScan.Click += btn_OneScan_Click;
        }

        void btn_OneScan_Click(object sender, EventArgs e)
        {
            if (!getConfigData())
            {
                MessageBox.Show("请完成配置文件修改");
                return;
            }
            //if (OneTrayScan())
            //{
            //    PLCLocationSet(PLCLocation, PLCLocation_OneTrayScanOKValue, out PLCSetReturn, out PLCSetMessage);
            //    if (PLCSetReturn == 1)
            //    {
            //        MessageShowADD(string.Format("第一次扫码成功，上位机（{0}）位（{1}）写入完成", PLCLocation, PLCLocation_OneTrayScanOKValue));
            //    }
            //    else
            //    {
            //        MessageShowADD(string.Format("第一次扫码成功，上位机（{0}）位（{1}）写入失败", PLCLocation, PLCLocation_OneTrayScanOKValue));
            //    }
            //}
        }






        #region void SerialPortRefresh()+  获得本机串口列表，查询配置文件中是否有参数保存。刷新HT_SerialPortDataList和CLB_SerialPortList内容

        /// <summary>
        /// 获得本机串口列表，查询配置文件中是否有参数保存。刷新HT_SerialPortDataList和CLB_SerialPortList内容
        /// </summary>
        void SerialPortRefresh()
        {
            //清空接口列表
            HT_SerialPortDataList.Clear();
            //获得本机串口名数组
            string[] strCom = SerialPort.GetPortNames();

            if (strCom == null)//判断本机是否有串口
            {
               MessageShowADD("本机没有串口！");
               return;
            }

            //循环获得本机每一个串口的串口名称
            foreach (string com in strCom)
            {
                //新建一个串口数据对象
                SerialPort sp = new SerialPort();
                //SerialPort SPData = new SerialPort();
                //对串口对象 串口名称赋值
                sp.PortName = com;
              //  SPData.PortName = com;

                //循环检查当前配置文件内是有记录的串口数据
                foreach (KeyValueConfigurationElement key in config.AppSettings.Settings)
                {
                    if (key.Key.Contains(com))//判断当前Settings对象是否为当前串口
                    {
                        if (key.Key.Contains("BaudRate"))
                        {
                            int value;
                            if (int.TryParse(key.Value, out value))
                            {
                                //将配置里面的波特率值赋值给串口对象
                                sp.BaudRate = value;
                                //SPData.BaudRate = value;
                            }

                        }
                        else if (key.Key.Contains("DataBits"))
                        {
                            int value;
                            if (int.TryParse(key.Value, out value))
                            {
                                //将配置里面的数据位值赋值给串口对象
                                sp.DataBits = value;
                                //SPData.DataBits = value;
                            }
                        }
                        else if (key.Key.Contains("StopBit"))
                        {
                            //将配置里面的停止位值赋值给串口对象
                            //SPData.StopBit = key.Value;
                       
                                 switch (key.Value)
                                {
                                    case "One":
                                        sp.StopBits = System.IO.Ports.StopBits.One;
                                        break;
                                    case "OnePointFive":
                                        sp.StopBits = System.IO.Ports.StopBits.OnePointFive;
                                        break;
                                    case "Two":
                                        sp.StopBits = System.IO.Ports.StopBits.Two;
                                        break;
                                    default:
                                        sp.StopBits = System.IO.Ports.StopBits.None;
                                        break;

                                }
                        }
                        else if (key.Key.Contains("Parity"))
                        {
                               //将配置里面的校验位值赋值给串口对象
                            //SPData.Parity = key.Value;
                            switch (key.Value)
                            {


                                case "Odd":
                                    sp.Parity = Parity.Odd;
                                    break;
                                case "Even":
                                    sp.Parity=Parity.Even;
                                    break;
                                default:
                                    sp.Parity = Parity.None;
                                    break;

                            }
                        }

                    }
                }
                
                HT_SerialPortDataList.Add(com, sp);
                this.CLB_SerialPortList.Items.Add(com);

            }


        }

        #endregion




        #region   void CLB_SerialPortList_ItemCheck(object sender, ItemCheckEventArgs e)+ 选中项改变事件 ，判断当前接口选项是否被勾选，勾选则打开串口连接，未勾选则断开串口连接

        /// <summary>
        /// 选中项改变事件 ，判断当前接口选项是否被勾选，勾选则打开串口连接，未勾选则断开串口连接
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CLB_SerialPortList_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (CLB_SerialPortList.GetItemCheckState(e.Index) == CheckState.Unchecked)//判断所选项的值是否为已勾选
            {
                ((SerialPort)HT_SerialPortDataList[CLB_SerialPortList.Text.ToString()]).Open();//勾选打开串口连接
                MessageShowADD(string.Format("打开{0}串口", CLB_SerialPortList.Text));
            }
            else
            {
                ((SerialPort)HT_SerialPortDataList[CLB_SerialPortList.Text.ToString()]).Close();//未勾选断开串口连接
                MessageShowADD(string.Format("关闭{0}串口", CLB_SerialPortList.Text));
            }
        } 
        #endregion



        #region btn_ConnPLC_Click(object sender, EventArgs e)   +连接PLC点击事件

        /// <summary>
        ///  连接PLC点击事件，点击实现连接PLC或者断开PLC连接   
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void btn_ConnPLC_Click(object sender, EventArgs e)
        {
            int ALSNumber;
            if (!int.TryParse(txt_ALSNumber.Text, out ALSNumber))
            {

                txt_MessageShow.AppendText("请输入正确的PLC工站号");
            }
            axActUtlType.ActLogicalStationNumber = ALSNumber;


            try
            {
                if (PLCConnState)
                {
                    int commRtn = this.axActUtlType.Close();
                    if (commRtn == 0)
                    {
                        PLCConnState = false;
                        btn_ConnPLC.Text = "连接PLC";
                        MessageShowADD(string.Format("PLC（{0}）断开成功", txt_ALSNumber.Text));
                        CycleIndex=-100;
                        CycleIndex2 = -100;
                            
                    }
                    else
                    {
                        txt_MessageShow.AppendText("断开失败");
                    }

                }
                else
                {

                    int commRtn = this.axActUtlType.Open();
                    if (commRtn == 0)
                    {
                        PLCConnState = true;
                        btn_ConnPLC.Text = "断开连接";
                        MessageShowADD(string.Format("PLC（{0}）连接成功", txt_ALSNumber.Text));
                    }
                    else
                    {
                        txt_MessageShow.AppendText("连接失败");
                    }


                }

            }
            catch (Exception ex)
            {

                txt_MessageShow.AppendText(ex.ToString());
            }

        }


        #endregion




        #region   void CLB_SerialPortList_SelectedIndexChanged(object sender, EventArgs e)+ CLB索引项改变事件,索引改变时在lbl中展示改接口对象数据

        /// <summary>
        ///  CLB索引项改变事件,索引改变时在lbl中展示改接口对象数据
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CLB_SerialPortList_SelectedIndexChanged(object sender, EventArgs e)
        {
            SerialPort sp = (SerialPort)HT_SerialPortDataList[CLB_SerialPortList.Text.ToString()];
            alterSerialPortData(sp);

        } 
        #endregion




        #region  Configuration GetConfigFile()+获得当前项目的配置文件对象

        /// <summary>
        /// 获得当前项目的配置文件对象
        /// </summary>
        /// <returns></returns>
        Configuration GetConfigFile()
        {
            //获得当前程序路径
            string file = System.Windows.Forms.Application.ExecutablePath;
            //通过路径访问配置文件
            return ConfigurationManager.OpenExeConfiguration(file);

        }

        #endregion



        #region  void getConfigData()+获得配置文件各项参数值

        /// <summary>
        /// 获得配置文件各项参数值
        /// </summary>
        bool getConfigData()
        {
            PLCLocation = config.AppSettings.Settings["PLCLocation"].Value;
            PLCLocation_ShelfScanValue = config.AppSettings.Settings["PLCLocation_ShelfScanValue"].Value;
            PLCLocation_ShelfScanOKValue = config.AppSettings.Settings["PLCLocation_ShelfScanOKValue"].Value;
            PLCLocation_ShelfScanNGValue = config.AppSettings.Settings["PLCLocation_ShelfScanNGValue"].Value;
            PLCLocation_OneTrayScanValue = config.AppSettings.Settings["PLCLocation_OneTrayScanValue"].Value;
            PLCLocation_OneTrayScanOKValue = config.AppSettings.Settings["PLCLocation_OneTrayScanOKValue"].Value;
            PLCLocation_OneTrayScanNGValue = config.AppSettings.Settings["PLCLocation_OneTrayScanNGValue"].Value;
            PLCLocation_TwoTrayScanValue = config.AppSettings.Settings["PLCLocation_TwoTrayScanValue"].Value;
            PLCLocation_TwoTrayScanOKValue = config.AppSettings.Settings["PLCLocation_TwoTrayScanOKValue"].Value;
            PLCLocation_TwoTrayScanNGValue = config.AppSettings.Settings["PLCLocation_TwoTrayScanNGValue"].Value;
            PLCLocation_TraySNScanValue = config.AppSettings.Settings["PLCLocation_TraySNScanValue"].Value;
            ShelfScanSP = config.AppSettings.Settings["ShelfScanSP"].Value;
           // ShelfScanDateInterval = config.AppSettings.Settings["ShelfScanDateInterval"].Value;
            OneTrayScanSP  = config.AppSettings.Settings["OneTrayScanSP"].Value;
            //OneTrayScanDateInterval = config.AppSettings.Settings["OneTrayScanDateInterval"].Value;
            TwoTrayScanSP = config.AppSettings.Settings["TwoTrayScanSP"].Value;
            //TwoTrayDateInterval = config.AppSettings.Settings["TwoTrayDateInterval"].Value;


            if (!HT_SerialPortDataList.ContainsKey(ShelfScanSP))
            {
                MessageShowADD(string.Format("并不存在{0}串口，请检查", ShelfScanSP));
                CycleIndex = -1;
                return false;
            }


            if (!HT_SerialPortDataList.ContainsKey(OneTrayScanSP))
            {
                MessageShowADD(string.Format("并不存在{0}串口，请检查", ShelfScanSP));
                CycleIndex = -1;
                return false;
            }

            if (!HT_SerialPortDataList.ContainsKey(TwoTrayScanSP))
            {
                MessageShowADD(string.Format("并不存在{0}串口，请检查", ShelfScanSP));
                CycleIndex = -1;
                return false;
            }


            if (!double.TryParse(config.AppSettings.Settings["OneTrayScanDateInterval"].Value, out OneTrayScanDateInterval))
            {
                MessageShowADD(string.Format("配置文件中的{OneTrayScanDateInterval}项(第一次扫描时间间隔)值有误，请确定是小数类型（double）"));
                CycleIndex = -1;//中断循环
                return false;
            }


            if (!double.TryParse(config.AppSettings.Settings["ShelfScanDateInterval"].Value, out ShelfScanDateInterval))
            {
                MessageShowADD(string.Format("配置文件中的{ShelfScanDateInterval}项(料架次扫描时间间隔)值有误，请确定是小数类型（double）"));
                CycleIndex = -1;//中断循环
                return false;
            }


            if (!double.TryParse(config.AppSettings.Settings["TwoTrayScanDateInterval"].Value, out TwoTrayScanDateInterval))
            {
                MessageShowADD(string.Format("配置文件中的{TwoTrayScanDateInterval}项(第二次扫描时间间隔)值有误，请确定是小数类型（double）"));
                CycleIndex = -1;//中断循环
                return false;
            }
            return true;

        }

        #endregion



        #region void MessageShowADD(string message) + 消息展示方法
        /// <summary>
        /// 消息展示方法
        /// </summary>
        /// <param name="message"></param>
        public void MessageShowADD(string message)
        {
            string a = string.Format(" {0} :{1}  {2}", DateTime.Now, message, System.Environment.NewLine);
            this.txt_MessageShow.AppendText(a);
            this.txt_View_MessageShow.AppendText(a);
        }


        #endregion



        #region void alterSerialPortData(SerialPort spdate)  +传入串口数据对象，将对象数据赋值到lbl

        /// <summary>
        /// 传入串口数据对象，将对象数据赋值到lbl
        /// </summary>
        /// <param name="spdate">接口数据对象</param>
        public void alterSerialPortData(SerialPort sp)
        {
            this.lbl_BaudRate.Text = "波特率:" + sp.BaudRate;
            this.lbl_DataBits.Text = "数据位:" + sp.DataBits;
            this.lbl_StopBits.Text = "停止位:" + sp.StopBits;
            this.lbl_Parity.Text = "校验位:" + sp.Parity;
        }
        #endregion




        #region   void btn_SerialPortDataAlter_Click(object sender, EventArgs e)   + 接口数据修改按钮点击事件
        /// <summary>
        /// 接口数据修改按钮点击事件 点击后打开接口对象数据修改窗口
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void btn_SerialPortDataAlter_Click(object sender, EventArgs e)
        {

            if (string.IsNullOrWhiteSpace(CLB_SerialPortList.Text))//判断是否选择串口
            {
                MessageBox.Show("请选择需修改的串口");
                return;
            }

            form_SPDataAlter form_spdateAlter = new form_SPDataAlter((SerialPort)HT_SerialPortDataList[CLB_SerialPortList.Text.ToString()]);


            MessageShowADD(string.Format("点击修改{0}接口参数", CLB_SerialPortList.Text));

            form_spdateAlter.Show();


        }

        #endregion





        //启动自动访问程序
        void btn_AtartAutoRun_Click(object sender, EventArgs e)
        {
            MessageShowADD("程序开始");
            PLCLocation = config.AppSettings.Settings["PLCLocation"].Value;//获得架子扫描的PLC位

            // MessageShowADD("开始PLC料架位检查");
             Func<string> F_Start = GetPLCLocation;
             IAsyncResult IR_Start = F_Start.BeginInvoke(null, null);



            //MessageShowADD("开始PLC料架位检查");
            //Func<string> F_ShelfScan = getPLCShelfScan;
            //IAsyncResult asyncResult = F_ShelfScan.BeginInvoke(null, null);


            //MessageShowADD("开始PLC第二次托盘扫描位检查");
            //Func<string> F_OnetrayScan = getPLCOnetrayScan;
            //IAsyncResult asyncResult2 = F_OnetrayScan.BeginInvoke(null, null);


            //MessageShowADD("开始PLC第二次托盘扫描位检查");
            //Func<string> F_TwotrayScan = getPLCTwotrayScan;
            //IAsyncResult asyncResult3 = F_TwotrayScan.BeginInvoke(null, null);

             
        }


        string GetPLCLocation() 
        {

            CycleIndex = 1;
            int PLCGetReturn;
            string PLCGetMessage;
            string PLCGetData;

             int PLCSetReturn;
             string PLCSetMessage;
            
            try
            {
                MessageShowADD(string.Format("PLC指令访问开始PLC指令位（{0}）",PLCLocation));
                while (CycleIndex>=1)
                {
                    Thread.Sleep(500);
                    PLCLocationGet(PLCLocation, out PLCGetReturn, out PLCGetMessage, out PLCGetData);
                    if (PLCGetReturn==-202)
                    {
                        MessageShowADD(PLCGetMessage);
                        CycleIndex = -202;//中断循环
                    }
                    else if(PLCGetReturn==1)
                    {
                        switch (PLCGetData)
                        {
                            case "1"://呼叫第一次料盘扫码
                                MessageShowADD("呼叫第一次料盘扫码");
                                if ( OneTrayScan())
                                {
                                    PLCLocationSet(PLCLocation, PLCLocation_OneTrayScanOKValue, out PLCSetReturn, out PLCSetMessage);
                                    if (PLCSetReturn==1)
                                    {
                                        MessageShowADD(string.Format("第一次扫码成功，上位机（{0}）位（{1}）写入完成", PLCLocation, PLCLocation_OneTrayScanOKValue));
                                    }
                                    else
                                    {
                                        MessageShowADD(string.Format("第一次扫码成功，上位机（{0}）位（{1}）写入失败", PLCLocation, PLCLocation_OneTrayScanOKValue));
                                    }
                                }
                                else
                                {
                                    PLCLocationSet(PLCLocation, PLCLocation_OneTrayScanNGValue, out PLCSetReturn, out PLCSetMessage);
                                    if (PLCSetReturn == 1)
                                    {
                                        MessageShowADD(string.Format("第一次扫码失败，上位机（{0}）位（{1}）写入完成", PLCLocation, PLCLocation_OneTrayScanNGValue));
                                    }
                                    else
                                    {
                                        MessageShowADD(string.Format("第一次扫码失败，上位机（{0}）位（{1}）写入失败", PLCLocation, PLCLocation_OneTrayScanNGValue));
                                    }
                                }
                                break;
                            case "2"://第一次料盘扫码成功
                                break;
                            case "3"://第一次料盘扫码失败
                                break;
                            case "4"://呼叫第二次料盘扫码
                                MessageShowADD("呼叫第二次料盘扫码");
                                if (TwoTrayScan())  
                                {
                                    PLCLocationSet(PLCLocation, PLCLocation_TwoTrayScanOKValue, out PLCSetReturn, out PLCSetMessage);
                                    if (PLCSetReturn == 1)
                                    {
                                        MessageShowADD(string.Format("第二次扫码成功，上位机（{0}）位（{1}）写入完成", PLCLocation, PLCLocation_TwoTrayScanOKValue));
                                    }
                                    else
                                    {
                                        MessageShowADD(string.Format("第二次扫码成功，上位机（{0}）位（{1}）写入失败", PLCLocation, PLCLocation_TwoTrayScanOKValue));
                                    }
                                }
                                else
                                {
                                    PLCLocationSet(PLCLocation, PLCLocation_TwoTrayScanNGValue, out PLCSetReturn, out PLCSetMessage);
                                    if (PLCSetReturn == 1)
                                    {
                                        MessageShowADD(string.Format("第二次扫码失败，上位机（{0}）位（{1}）写入完成", PLCLocation, PLCLocation_TwoTrayScanNGValue));
                                    }
                                    else
                                    {
                                        MessageShowADD(string.Format("第二次扫码失败，上位机（{0}）位（{1}）写入失败", PLCLocation, PLCLocation_TwoTrayScanNGValue));
                                    }
                                }
	                      break;
                            case "5"://第二次料盘扫码成功
                                break;
                            case "6"://第二次料盘扫码失败    
                                break;      
                            case "64"://PLC料架到位信号为1 ，相应料架扫码枪串口
                                MessageShowADD("呼叫料架扫码");
                                SetShelfScan();
                                break;
                            case "8"://PLC料架扫码成功
                                break;
                            case "9"://PLC料架扫码失败
                                break;
                            case "10"://托盘扫码成功
                                break;
                            default:
                                break;
                        }
                    }

                 

                }

                return null;
            }
            catch (Exception ex)
            {

                MessageShowADD(ex.ToString());
                return null;
            }
        
        
        }







        #region getPLCShelfScan()+ 获得PLC架子扫码位内容

        /// <summary>
        /// 获得PLC架子扫码位内容
        /// </summary>
        /// <returns></returns>
        string getPLCShelfScan()
        {

            string PLCShelfScan = config.AppSettings.Settings["PLCShelfScan"].Value;//获得架子扫描的PLC位
            int PLCShelfScan_Return;
            string PLCShelfScan_Message;
         
            string PLCShelfScandata;//获得架子扫描的PLC指令数据结果

            CycleIndex = 0;//循环检索字段
            try
            {

                while (CycleIndex>-1)//循环读取
                {
                  
                    Thread.Sleep(500);//每次访问PLC时间间隔为0.5秒
                   
                        this.PLCLocationGet(PLCLocation, out PLCShelfScan_Return, out PLCShelfScan_Message, out PLCShelfScandata);

                        if (PLCShelfScan_Return ==1)//数据结果等于0表示查询成功
                        {
                            if (PLCShelfScandata.ToString() == "1")//获得扫码结果判断是否为1
                            {

                                MessageShowADD(string.Format("PLC{0}位为1,呼叫串口执行料架扫描。", PLCShelfScan));
                                //执行响应料架扫码串口
                                SetShelfScan();


                                PLCLocationSet(PLCShelfScan, "1", out PLCShelfScan_Return, out PLCShelfScan_Message);


                                if (PLCShelfScan_Return == 1)
                                {
                                    MessageShowADD(string.Format("上位机料架扫码位（{0}）置位1成功", PLCShelfScan));
                                }
                                else
                                {
                                    MessageShowADD(string.Format("上位机料架扫码位（{0}）置位1失败", PLCShelfScan));
                                }
                            }
                        }
                  

                }
                // SetShelfScan();
                return null;

            }
            catch (Exception ex)
            {
                MessageShowADD(ex.ToString());
                return null;
            }


        }

        #endregion



        #region  void SetShelfScan()   + 向料架扫描枪发送 "+ "  定义线程接收料架扫描枪返回扫码内容，线程方法：getSPScanData(SerialPort sp)  回调方法:SP_ShelfScanOK

        /// <summary>
        /// 向料架扫描枪发送 "+ "  定义线程接收料架扫描枪返回扫码内容
        /// </summary>
        private void SetShelfScan()
        {
            
            //判断料架扫码枪串口是否已开启
            if (((SerialPort)HT_SerialPortDataList[(config.AppSettings.Settings["ShelfScanSP"].Value)]) .IsOpen)
            {

            
                ((SerialPort)HT_SerialPortDataList[(config.AppSettings.Settings["ShelfScanSP"].Value)]) .Write("+" +System.Environment.NewLine);
              

                //定义线程接受料架扫描枪返回值内容
                Func<SerialPort, string> axc = getSPScanData;

                IAsyncResult asyncResult = axc.BeginInvoke((SerialPort)HT_SerialPortDataList[(config.AppSettings.Settings["ShelfScanSP"].Value)], SP_ShelfScanOK, null);

                return;
            }   
            else
            {
                MessageShowADD("串口没打开");
                CycleIndex = -1;
            }

        }




        /// <summary>
        /// 接收料架扫码枪返回内容的多线程方法
        /// </summary>
        /// <param name="sp"></param>
        /// <returns></returns>
        string getSPScanData(SerialPort sp)
        {
            DateTime BeginDate = DateTime.Now;
            DateTime EndDate;
            string SPReturnData = null;

            while (string.IsNullOrWhiteSpace(SPReturnData))
            {

                SPReturnData = sp .ReadExisting();
                EndDate = DateTime.Now;
                TimeSpan oTime = EndDate.Subtract(BeginDate);
              
                if (oTime.TotalSeconds > ShelfScanDateInterval)
                {
                    MessageShowADD(string.Format("{0}扫描枪扫描超时，如需修改超时时限请修改配置文件ShelfScanDate项", sp.PortName));
                    CycleIndex = -1;
                    return null;
                }

            }
            return SPReturnData;

        }

       

        /// <summary>
        /// 料架扫码完成后回调函数。 
        /// </summary>
        /// <param name="iar"></param>
        void SP_ShelfScanOK(IAsyncResult iar)
        {

            AsyncResult ar = (AsyncResult)iar;
        
            Func<SerialPort, string> Func = (Func<SerialPort, string>)ar.AsyncDelegate;

            shelfSN = Func.EndInvoke(iar);
            int dataRtn;
            string PLCSetMessage;

            if (string.IsNullOrWhiteSpace(shelfSN))//如果扫码返回空则，扫码超时
            {
                //扫码失败写入
                PLCLocationSet(PLCLocation, PLCLocation_ShelfScanNGValue, out dataRtn, out PLCSetMessage);
                //dataRtn = this.axActUtlType.WriteDeviceBlock2(PLCLocation, Convert.ToInt32(PLCLocation_ShelfScanOKValue), ref data);
                
                if (dataRtn == 1)
                { //扫码失败写入成功
                    MessageShowADD(string.Format("料架扫码超时，上位机（{0}）位（{1}）写入成功", PLCLocation, PLCLocation_ShelfScanNGValue));
                }
                else
                {//扫码失败写入失败
                    MessageShowADD(string.Format("料架扫码超时，上位机（{0}）位（{1}）写入失败", PLCLocation, PLCLocation_ShelfScanNGValue));
                    CycleIndex = -1;
                }
            }
            else if (shelfSN.ToLower().Contains("ng"))
            {
                PLCLocationSet(PLCLocation, PLCLocation_ShelfScanNGValue, out dataRtn, out PLCSetMessage);

                if (dataRtn == 1)
                {
                    MessageShowADD(string.Format("扫码失败，上位机（{0}）位（{1}）写入成功", PLCLocation, PLCLocation_ShelfScanNGValue));
                }
                else
                {
                    MessageShowADD(string.Format("扫码失败，上位机（{0}）位（{1}）写入失败", PLCLocation, PLCLocation_ShelfScanNGValue));
                    CycleIndex = -1;
                }

            }
            else
            {
                PLCLocationSet(PLCLocation, PLCLocation_ShelfScanOKValue, out dataRtn, out PLCSetMessage);

                if (dataRtn == 1)
                {
                    MessageShowADD(string.Format("扫码成功 料架SN：{2};上位机（{0}）位（{1}）写入成功", PLCLocation, PLCLocation_ShelfScanOKValue,shelfSN));
                }
                else
                {
                    MessageShowADD(string.Format("扫码成功 料架SN：{2};上位机（{0}）位（{1}）写入失败", PLCLocation, PLCLocation_ShelfScanOKValue, shelfSN));
                }

                //将料架SN赋值
                txt_ShelfSN.Text = shelfSN;


            }


        }


        #endregion


  

        bool OneTrayScan() 
        {
            MessageShowADD("开始执行第一次托盘扫描");
            int SPReturn = -1;
            string SPMessage = null;
            string SPData = null;

            SerialPort SP = (SerialPort)HT_SerialPortDataList[(config.AppSettings.Settings["OneTrayScanSP"].Value)];

            //向串口发送BLOAD,5
            SerialPortSet(SP , "BLOAD,5", out SPReturn, out SPMessage);
            if (SPReturn != 1)
            {
                MessageShowADD(string.Format("{0}串口发送数据{1}发生错误，原因{2}：", SP.PortName, "BLOAD,5", SPMessage));
                CycleIndex = -1;//中断循环
                return false;
            }
            else
            {
                MessageShowADD(string.Format("{0}串口发送数据{1}发生成功", SP.PortName, "BLOAD,5"));
            }


            //接收串口信息判断是否为OK
            SerialPortGet(SP , OneTrayScanDateInterval, out SPReturn, out SPMessage, out SPData);
            if (SPReturn != 1)
            {
                MessageShowADD(string.Format("{0}串口接受数据发生错误，原因{1}：", SP.PortName, SPMessage));
                CycleIndex = -1;//中断循环
                return false;
            }
            else if (SPData.Trim().ToLower() != "ok")
            {
                MessageShowADD(string.Format("{0}串口返回ng，原因{2}：", SP.PortName, SPMessage));
                CycleIndex = -1;//中断循环
                return false;
            }
            else
            {
                MessageShowADD(string.Format("{0}串口返回ok", SP.PortName));
            }






            //向串口发送BLOAD,5
            SerialPortSet(SP , "LON", out SPReturn, out SPMessage);
            if (SPReturn != 1)
            {
                MessageShowADD(string.Format("{0}串口发送数据{1}发生错误，原因{2}：", SP.PortName, "LON", SPMessage));
                CycleIndex = -1;//中断循环
                return false;
            }
            else
            {
                MessageShowADD(string.Format("{0}串口发送数据{1}成功", SP.PortName, "LON"));
            }


            //向串口接收托盘信息
            SerialPortGet(SP , OneTrayScanDateInterval, out SPReturn, out SPMessage, out SPData);
            if (SPReturn != 1)
            {
                MessageShowADD(string.Format("{0}串口接受数据发生错误，原因{1}：", SP.PortName, SPMessage));
                CycleIndex = -1;//中断循环
                return false;
            }
            else
            {
                MessageShowADD(string.Format("{0}串口接受数据成功，数据详情：{1}", SP.PortName, SPData));
            }



            //显示刷新托盘数据表
            SPGetDataAddTable(true, false, SPData, out SPReturn, out SPMessage);
            if (SPReturn == -202)
            {
                MessageShowADD(string.Format("第二次托盘扫描数据显示失败 原因：{0} ；", SPMessage));
                CycleIndex = -1;//中断循环
                return false;
            }
            else
            {
                MessageShowADD(string.Format("第二次托盘扫描完成 ；"));
            }


            return true;





        }
        






        #region getPLCOnetrayScan()   + 第二次位置扫描

        /// <summary>
        /// 第二次位置扫描
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public string getPLCOnetrayScan()
        {
            string PLCOnetrayScanLocation = config.AppSettings.Settings["PLCLocation_OneTrayScan"].Value;//获第二次托盘扫描扫描的PLC位
            string NGaddr = config.AppSettings.Settings["PLCLocation_OneTrayScanOK"].Value;//扫码NG的PLC位置号
            string OKaddr = config.AppSettings.Settings["PLCLocation_OneTrayScanNG"].Value;//扫码ok的PLC位置号






             CycleIndex = 0;//循环检索字段

         

            int OnetrayScanPLCGetReturn = -1;
            string OnetrayScanPLCGetMessage = null;
            string OnetrayScanPLCGetData = null;

            int OnetrayScaPLCSetReturn;
            string OnetrayScanPLCSetMessage = null;

            int SPSetReturn=-1;
            string SPSetMessage = null;

            int SPGetReturn=-1;
            string SPGetMessage = null;
            string SPGetData;

            



            MessageShowADD(string.Format("开始判断PLC是否呼叫第二次托盘扫描，检索PLC （{0}）位数据" ,PLCOnetrayScanLocation));

            while (CycleIndex > -1)
            {
                Thread.Sleep(500);
                //查询第二次扫描位置PLC位号数据是否为内容
                PLCLocationGet(PLCOnetrayScanLocation, out OnetrayScanPLCGetReturn, out OnetrayScanPLCGetMessage, out OnetrayScanPLCGetData);

                if (OnetrayScanPLCGetReturn == -202)//查询结果为-202发生错误
                { 
                    MessageShowADD("第二次托盘扫描PLC数据查询："+ OnetrayScanPLCGetMessage);
                    CycleIndex = -1;//中断循环
                }
                else if (OnetrayScanPLCGetReturn == 1 && OnetrayScanPLCGetData=="1") //查询结果为1/PLC呼叫扫码
                {
                   
                    MessageShowADD("PLC呼叫执行第二次托盘扫描");
                    //将第二次扫描位置PLC位号数据内容置位为0
                    PLCLocationSet(PLCOnetrayScanLocation, "0", out OnetrayScaPLCSetReturn, out OnetrayScanPLCSetMessage);

                    MessageShowADD(string.Format("对PLC （{0}）位数据进行置位为0", PLCOnetrayScanLocation));

                    if (OnetrayScaPLCSetReturn!=1)
                    {
                        MessageShowADD("第二次托盘扫描PLC置位0失败：" + OnetrayScanPLCSetMessage);
                        CycleIndex = -1;//中断循环
                      
                    }
                    else
                    {
                        MessageShowADD("第二次托盘扫描PLC置位成功：" + OnetrayScanPLCSetMessage);
                    }
                  


                   SerialPort SP=   (SerialPort)HT_SerialPortDataList[(config.AppSettings.Settings["ShelfScanSP"].Value)];


                   SerialPortSet(SP , "BLOAD,5", out SPSetReturn, out SPSetMessage);
                   if (SPSetReturn!=1)
                   {
                       MessageShowADD(string.Format("{0}串口发送数据{1}发生错误，原因{2}：", SP.PortName, "BLOAD,5",SPSetMessage));
                       CycleIndex = -1;//中断循环
                   }
                   else
                   {
                       MessageShowADD(string.Format("{0}串口发送数据{1}发生成功", SP.PortName, "BLOAD,5"));
                   }

                   



                   SerialPortGet(SP , OneTrayScanDateInterval, out SPGetReturn, out SPGetMessage, out SPGetData);
                   if (SPGetReturn!=1)
                   {
                       MessageShowADD(string.Format("{0}串口接受数据发生错误，原因{1}：", SP.PortName, SPGetMessage));
                       CycleIndex = -1;//中断循环
                   }
                   else if (SPGetData.Trim().ToLower()!="ok")
                   {
                       MessageShowADD(string.Format("{0}串口返回ng，原因{2}：", SP.PortName, SPGetMessage));
                       CycleIndex = -1;//中断循环
                   }
                   else
                   {
                       MessageShowADD(string.Format("{0}串口返回ok", SP.PortName));
                   }







                   SerialPortSet(SP , "LON", out SPSetReturn, out SPSetMessage);
                   if (SPSetReturn != 1)
                   {
                       MessageShowADD(string.Format("{0}串口发送数据{1}发生错误，原因{2}：", SP.PortName, "LON", SPSetMessage));
                       CycleIndex = -1;//中断循环
                   }
                   else
                   {
                       MessageShowADD(string.Format("{0}串口发送数据{1}成功", SP.PortName, "LON"));
                   }



                   SerialPortGet(SP , OneTrayScanDateInterval, out SPGetReturn, out SPGetMessage, out SPGetData);
                   if (SPGetReturn != 1)
                   {
                       MessageShowADD(string.Format("{0}串口接受数据发生错误，原因{1}：", SP.PortName, SPGetMessage));
                       CycleIndex = -1;//中断循环
                   }
                   else
                   {
                       MessageShowADD(string.Format("{0}串口接受数据成功，数据详情：{1}", SP.PortName, SPGetData));
                   }

                   
                   SPGetDataAddTable(true,false,SPGetData,out P_Return,out P_Message);

                   if (P_Return==-202)
                   {
                       MessageShowADD(string.Format("第二次托盘扫描数据显示失败 原因：{0} ；", P_Message));
                       CycleIndex = -1;//中断循环
                   }
                   else
                   {
                       MessageShowADD(string.Format("第二次托盘扫描数据获取与显示完成 ；"));
                   }
              



                }
               
                
            }

            return null;


        }

        #endregion



        bool TwoTrayScan() 
        {
            MessageShowADD("开始执行第二次托盘扫描");
            int SPReturn = -1;
            string SPMessage = null;
            string SPData = null;

            SerialPort SP = (SerialPort)HT_SerialPortDataList[TwoTrayScanSP];

            //向串口发送BLOAD,6
            SerialPortSet(SP , "BLOAD,6", out SPReturn, out SPMessage);
            if (SPReturn != 1)
            {
                MessageShowADD(string.Format("{0}串口发送数据{1}发生错误，原因{2}：", SP.PortName, "BLOAD,5", SPMessage));
                CycleIndex = -1;//中断循环
                return false;
            }
            else
            {
                MessageShowADD(string.Format("{0}串口发送数据{1}发生成功", SP.PortName, "BLOAD,6"));
            }


            //接收串口信息判断是否为OK
            SerialPortGet(SP , TwoTrayScanDateInterval, out SPReturn, out SPMessage, out SPData);
            if (SPReturn != 1)
            {
                MessageShowADD(string.Format("{0}串口接受数据发生错误，原因{1}：", SP.PortName, SPMessage));
                CycleIndex = -1;//中断循环
                return false;
            }
            else if (SPData.Trim().ToLower() != "ok")
            {
                MessageShowADD(string.Format("{0}串口返回ng，原因{2}：", SP.PortName, SPMessage));
                CycleIndex = -1;//中断循环
                return false;
            }
            else
            {
                MessageShowADD(string.Format("{0}串口返回ok", SP.PortName));
            }






            //向串口发送LON
            SerialPortSet(SP , "LON", out SPReturn, out SPMessage);
            if (SPReturn != 1)
            {
                MessageShowADD(string.Format("{0}串口发送数据{1}发生错误，原因{2}：", SP.PortName, "LON", SPMessage));
                CycleIndex = -1;//中断循环
                return false;
            }
            else
            {
                MessageShowADD(string.Format("{0}串口发送数据{1}成功", SP.PortName, "LON"));
            }


            //向串口接收托盘信息
            SerialPortGet(SP , TwoTrayScanDateInterval, out SPReturn, out SPMessage, out SPData);
            if (SPReturn != 1)
            {
                MessageShowADD(string.Format("{0}串口接受数据发生错误，原因{1}：", SP.PortName, SPMessage));
                CycleIndex = -1;//中断循环
                return false;
            }
            else
            {
                MessageShowADD(string.Format("{0}串口接受数据成功，数据详情：{1}", SP.PortName, SPData));
            }



            //显示刷新托盘数据表
            SPGetDataAddTable(false, true, SPData, out SPReturn, out SPMessage);
            if (SPReturn == -202)
            {
                MessageShowADD(string.Format("第二次托盘扫描数据显示失败 原因：{0} ；", SPMessage));
                CycleIndex = -1;//中断循环
                return false;
            }
            else
            {
                MessageShowADD(string.Format("第二次托盘扫描完成 ；"));
            }


            return true;



              

        }





        #region getPLCTwotrayScan()   + 第二次位置扫描

        /// <summary>
        /// 第二次位置扫描
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public string getPLCTwotrayScan()
        {
            string PLCTwotrayScanLocation = config.AppSettings.Settings["PLCLocation_TwoTrayScan"].Value;//获第二次托盘扫描扫描的PLC位
            string NGaddr = config.AppSettings.Settings["PLCLocation_TwoTrayScanOK"].Value;//扫码NG的PLC位置号
            string OKaddr = config.AppSettings.Settings["PLCLocation_TwoTrayScanNG"].Value;//扫码ok的PLC位置号


             CycleIndex2 = 0;

            double TwotrayScanDateInterval;

            int TwotrayScanPLCGetReturn = -1;
            string TwotrayScanPLCGetMessage = null;
            string TwotrayScanPLCGetData = null;

            int TwotrayScaPLCSetReturn;
            string TwotrayScanPLCSetMessage = null;

            int SPSetReturn = -1;
            string SPSetMessage = null;

            int SPGetReturn = -1;
            string SPGetMessage = null;
            string SPGetData;





            MessageShowADD(string.Format("开始判断PLC是否呼叫第二次托盘扫描，检索PLC （{0}）位数据", PLCTwotrayScanLocation));

            while (CycleIndex2 > -1)
            {
                Thread.Sleep(500);
                //查询第二次扫描位置PLC位号数据是否为内容
                PLCLocationGet(PLCTwotrayScanLocation, out TwotrayScanPLCGetReturn, out TwotrayScanPLCGetMessage, out TwotrayScanPLCGetData);

                if (TwotrayScanPLCGetReturn == -202)//查询结果为-202发生错误
                {
                    MessageShowADD("第二次托盘扫描PLC数据查询：" + TwotrayScanPLCGetMessage);
                    CycleIndex2 = -1;//中断循环
                }
                else if (TwotrayScanPLCGetReturn == 1 && TwotrayScanPLCGetData == "1") //查询结果为1/PLC呼叫扫码
                {
                    //将第二次扫描位置PLC位号数据内容置位为0
                    MessageShowADD("PLC呼叫执行第二次托盘扫描");

                    PLCLocationSet(PLCTwotrayScanLocation, "0", out TwotrayScaPLCSetReturn, out TwotrayScanPLCSetMessage);

                    MessageShowADD(string.Format("对PLC （{0}）位数据进行置位为0", PLCTwotrayScanLocation));
                    if (TwotrayScaPLCSetReturn != 1)
                    {
                        MessageShowADD("第二次托盘扫描PLC置位失败：" + TwotrayScanPLCSetMessage);
                        CycleIndex2 = -1;//中断循环

                    }
                    else
                    {
                        MessageShowADD("第二次托盘扫描PLC置位成功：" + TwotrayScanPLCSetMessage);
                    }



                    SerialPort SP = (SerialPort)HT_SerialPortDataList[(config.AppSettings.Settings["ShelfScanSP"].Value)];


                    SerialPortSet(SP , "BLOAD,6", out SPSetReturn, out SPSetMessage);
                    if (SPSetReturn != 1)
                    {
                        MessageShowADD(string.Format("{0}串口发送数据{1}发生错误，原因{2}：", SP.PortName, "BLOAD,5", SPSetMessage));
                        CycleIndex2 = -1;//中断循环
                    }
                    else
                    {
                        MessageShowADD(string.Format("{0}串口发送数据{1}发生成功", SP.PortName, "BLOAD,5"));
                    }



                    if (!double.TryParse(config.AppSettings.Settings["TwotrayScanDateInterval"].Value, out TwotrayScanDateInterval))
                    {
                        MessageShowADD(string.Format("配置文件中的{TwotrayScanDateInterval}项(第二次扫描时间间隔)值有误，请确定是小数类型（double）"));
                        CycleIndex2= 1;//中断循环
                    }



                    SerialPortGet(SP , TwotrayScanDateInterval, out SPGetReturn, out SPGetMessage, out SPGetData);
                    if (SPGetReturn != 1)
                    {
                        MessageShowADD(string.Format("{0}串口接受数据发生错误，原因{1}：", SP.PortName, SPGetMessage));
                        CycleIndex2 = -1;//中断循环
                    }
                    else if (SPGetData.Trim().ToLower() != "ok")
                    {
                        MessageShowADD(string.Format("{0}串口返回ng，原因{2}：", SP.PortName, SPGetMessage));
                        CycleIndex2 = -1;//中断循环
                    }
                    else
                    {
                        MessageShowADD(string.Format("{0}串口返回ok", SP.PortName));
                    }







                    SerialPortSet(SP , "LON", out SPSetReturn, out SPSetMessage);
                    if (SPSetReturn != 1)
                    {
                        MessageShowADD(string.Format("{0}串口发送数据{1}发生错误，原因{2}：", SP.PortName, "LON", SPSetMessage));
                        CycleIndex2 = -1;//中断循环
                    }
                    else
                    {
                        MessageShowADD(string.Format("{0}串口发送数据{1}成功", SP.PortName, "LON"));
                    }



                    SerialPortGet(SP , TwotrayScanDateInterval, out SPGetReturn, out SPGetMessage, out SPGetData);
                    if (SPGetReturn != 1)
                    {
                        MessageShowADD(string.Format("{0}串口接受数据发生错误，原因{1}：", SP.PortName, SPGetMessage));
                        PLCLocationSet(NGaddr, "1", out P_Return, out P_Message);
                        if (P_Return!=1)
                        {
                            MessageShowADD(string.Format("第二次托盘扫描失败位（{0}）PLC置位失败：{1}", NGaddr, P_Message));
                        }
                        else
                        {
                            MessageShowADD(string.Format("第二次托盘扫描失败位（{0}）PLC置位成功：",NGaddr)); 
                        }
                       
                        CycleIndex2 = -1;//中断循环
                    }
                    else
                    {
                        MessageShowADD(string.Format("{0}串口接受数据成功，数据详情：{1}", SP.PortName, SPGetData));

                        PLCLocationSet(OKaddr, "1", out P_Return, out P_Message);
                        if (P_Return != 1)
                        {
                            MessageShowADD(string.Format("第二次托盘扫描成功位（{0}）PLC置位失败：{1}",OKaddr,P_Message));
                        }
                        else
                        {
                            MessageShowADD(string.Format("第二次托盘扫描失败位（{0}）PLC置位成功：", OKaddr));
                        }
                    }



                    SPGetDataAddTable(false,true,SPGetData, out P_Return, out P_Message);





                  
                    if (P_Return == -202)
                    {
                        MessageShowADD(string.Format("第二次托盘扫描数据获取与显示数据显示失败 原因：{0} ；", P_Message));
                        CycleIndex2 = -1;//中断循环
                    }
                    else
                    {
                        MessageShowADD(string.Format("第二次托盘扫描数据获取与显示完成 ；"));
                    }   

                }







            }

            return null;


        }

        #endregion






        #region   PLCLocationGet(string PLCLocation, out int PLCGetReturn,out string PLCGetMessage,out string PLCGetData)+传入PLC位号 ，返回PLC查询结果和消息,数据  PLCGetReturn= -202：发生错误

        /// <summary>
        /// 传入PLC位号 ，返回PLC查询结果和消息,数据   PLCGetReturn==1：数据检索成功   PLCGetReturn= -202：发生错误
        /// </summary>
        /// <param name="PLCLocation">PLC位号</param>
        /// <param name="PLCGetReturn">查询结果 （202：发生错误）</param>
        /// <param name="PLCGetMessage">查询消息</param>
        /// <param name="PLCGetData">数据</param>
        void PLCLocationGet(string PLCLocation, out int PLCGetReturn,out string PLCGetMessage,out string PLCGetData)
        {

            short[] GetPLCdata = new short[1];  //获得PLC指令数据结果
            PLCGetData = null;
            int num = 1;
            try
            {
                if (PLCConnState)//判断PLC的连接状况
                {
                    //查询数据位查看其结果
                    int dataRtn = this.axActUtlType.ReadDeviceBlock2(PLCLocation, num, out GetPLCdata[0]);

                    //判断查询数据返回是否为0  0代表查询成功 
                    if (dataRtn == 0)
                    {
                        PLCGetReturn = 1;
                        PLCGetMessage =string.Format("PLC（{0}）位查询成功", PLCLocation);
                        PLCGetData = GetPLCdata[0].ToString();
                    }
                    else
                    {
                        PLCGetReturn = -202;
                        PLCGetMessage= string.Format("PLC（{0}）位查询失败", PLCLocation);
                    }
                }
                else//判断得到PLC连接情况为false
                {
                    PLCGetReturn = -202;
                    PLCGetMessage= "PLC未连接";
                }

            }
            catch (Exception ex)
            {
                PLCGetReturn = -202;
                PLCGetMessage= ex.ToString();
            }

        } 
        #endregion


        #region  string PLCLocationSet(string PLCLocation, string PLCSetData, out int PLCSetReturn)+ 传入PLC写入位号，PLC写入内容，返回PLC写入结果和消息 成功，0 ：失败，-202 ：产生错误

        /// <summary>
        /// 传入PLC写入位号，PLC写入内容，返回PLC写入结果和消息 成功，1 ：失败，-202 ：产生错误
        /// </summary>
        /// <param name="PLCLocation">PLC写入位号</param>
        /// <param name="PLCSetData">PLC写入内容</param>
        /// <param name="PLCSetReturn">返回写入结果1 ：成功，1 ：失败，-202 ：产生错误</param>
        /// <returns>写入消息</returns>
        void PLCLocationSet(string PLCLocation, string PLCSetData, out int PLCSetReturn,out string PLCSetMessage)
        {
            PLCSetMessage = null;
            int PLCReturn = -202;
            short PLCdata = short.Parse(PLCSetData);
            try
            {
                if (PLCConnState)//判断PLC的连接状况
                {

                    PLCReturn = this.axActUtlType.WriteDeviceBlock2(PLCLocation, 1, ref PLCdata);
                    if (PLCReturn == 0)
                    {
                        PLCSetReturn = 1;
                        PLCSetMessage= string.Format("PLC（{0}）位写入成功", PLCLocation);
                    }
                    else
                    {
                        PLCSetReturn = 0;
                        PLCSetMessage= string.Format("PLC（{0}）位写入失败", PLCLocation);
                        CycleIndex = -1;
                    }
                }
                else//判断得到PLC连接情况为false
                {
                    PLCSetReturn = -202;
                    PLCSetMessage= "PLC未连接";
                    CycleIndex = -1;
                }
            }
            catch (Exception ex)
            {

                PLCSetReturn = -202;
                PLCSetMessage= ex.ToString();
                CycleIndex = -1;
            }


        }
        
        #endregion



        /// <summary>
        /// 向指定的串口对象发送数据，返回结果与消息，SPSetReturn=1：成功，SPSetReturn=-202 ：发生错误
        /// </summary>
        /// <param name="SPObject">串口对象</param>
        /// <param name="SPSetData">发送至串口对象的数据</param>
        /// <param name="SPSetReturn">结果</param>
        /// <param name="SPSetMessage">消息</param>
        void SerialPortSet(SerialPort SPObject,string SPSetData,out int SPSetReturn,out string SPSetMessage) 
        {
            try
            {
                if (!SPObject.IsOpen)
                {
                    SPSetReturn = -202;
                    SPSetMessage = string.Format("串口未连接");
                    CycleIndex = -1;
                    return;
                }

                SPObject.Write(SPSetData);


                SPSetReturn = 1;
                SPSetMessage=  string.Format("串口发送数据（{0}）完成", SPSetData);
                return;

            }
            catch (Exception ex )
            {

                SPSetReturn =-202;
                SPSetMessage = string.Format("串口发送数据发生错误，原因：{0}", ex.ToString());
                CycleIndex = -1;
            }
          

        }




        /// <summary>
        /// 在指定时限内向指定串口对象查询数据，返回查询结果，消息，数据 (1：成功,-202：发生错误)
        /// </summary>
        /// <param name="SPObject">串口对象</param>
        /// <param name="Interval">时限（多少秒内完成）</param>
        /// <param name="SPGetReturn">查询结果</param>
        /// <param name="SPGetMessage">消息</param>
        /// <param name="SPGetData">查询数据</param>
        void SerialPortGet(SerialPort SPObject, double Interval,out int SPGetReturn, out string SPGetMessage,out string SPGetData) 
        {
            DateTime BeginDate = DateTime.Now;
            DateTime EndDate;
            SPGetData = null;

            try
            {
                while (string.IsNullOrWhiteSpace(SPGetData))
                {
                    SPGetData = SPObject.ReadExisting();

                    EndDate = DateTime.Now;
                    TimeSpan TimeInterval = EndDate.Subtract(BeginDate);

                    if (TimeInterval.TotalSeconds > Interval)
                    {
                        SPGetReturn = -202;
                        SPGetMessage = (string.Format("扫描枪扫描超时"));
                        CycleIndex = -1;
                        return;

                    }

                }

                SPGetReturn = 1;
                SPGetMessage = string.Format("扫描成功");

                return;
            }
            catch (Exception ex)
            {
                SPGetReturn = -202;
                SPGetMessage = string.Format("串口数据接收发生错误，原因：{0}", ex.ToString());
                CycleIndex = -1;
            }
          

        }




        /// <summary>
        ///  判断是否将原数据清空，分割由串口得到的正确数据库并且显示在界面
        /// </summary>
        /// <param name="IsRefresh">判断是否将原数据清空</param>
        /// <param name="SPRtndata">所需显示数据</param>
        /// <param name="Retrun">结果</param>
        /// <param name="Message">消息</param>
        void SPGetDataAddTable( bool IsRefresh, bool AllFinish,  string SPRtndata,out int Retrun,out string Message ) 
        {
            if (IsRefresh)
            {
                DT_Data.Clear();
            }

            Retrun = -1;
            Message = null;
            //新建一个数组对象用于保存扫码枪返回数据后以,分割的每一个  SN:Location
            string[] SPRtnData = SPRtndata.Split(new char[] { ',' });
            try
            {
               
                //循环每一个SN:Location数组
                foreach (string data in SPRtnData)
                {
                    //以表对象新建一个对应的行对象
                    DataRow dr = DT_Data.NewRow();
                    //分割每一个SN:Location
                    string[] SNAndLocation = data.Split(new char[] { ':' });
                    //将分割后结果赋值到行对象那个中
                    dr["SN"] = Convert.ToInt32( SNAndLocation[0]);
                    dr["Location"] = SNAndLocation[1];
                    //将航对象添加入表
                    DT_Data.Rows.Add(dr); 
                }
                DT_Data.DefaultView.Sort = "Location ASC";
                DT_Data = DT_Data.DefaultView.ToTable();
                DGV_NewData.DataSource = DT_Data;

                
                if (AllFinish)
                {
                   // PLCLocation_TraySNScan = config.AppSettings.Settings["PLCLocation_TraySNScan"].ToString ();
                    traySN = DT_Data.Rows[DT_Data.Rows.Count - 1]["SN"].ToString ();
                    if (traySN.ToLower().Trim() != "ng" || string.IsNullOrWhiteSpace(traySN))
                    {
                        PLCLocationSet("D22", "1", out P_Return, out P_Message);

                        if (P_Return!=1)
                        {
                            Retrun= P_Return;
                            Message = P_Message;
                            return;
                        }
                       
                    }
                    else
                    {
                        Retrun = -202;
                        Message = "托盘SN获取失败";
                        CycleIndex = -1;
                        return;
                    }
                }
 



                Retrun = 1;
            }
            catch (Exception ex)
            {

                Retrun = -202;
                Message = ex.ToString();
                CycleIndex = -1;
            }
        
        
        }


       
        












    }
}
