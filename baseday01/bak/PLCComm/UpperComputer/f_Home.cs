﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO.Ports;

 
namespace UpperComputer
{
    public partial class f_Home : Form
    {
        int PLCWorkNumber=-1;
        bool b_PLCConState = false;
        PLCControl plccton = new PLCControl();
        SerialPort AlterSP;

        /// <summary>
        /// 串口字典
        /// </summary>
        Dictionary<string, SerialPort> Dic_SerialPort = new Dictionary<string, SerialPort>();
        
        /// <summary>
        /// 串口控制对象
        /// </summary>
        SerialPortControl spc = new SerialPortControl();
        

        public f_Home()
        {
            InitializeComponent();

            btn_ComPLC.Click += btn_ComPLC_Click;

            Dic_SerialPortRefresh();

           // this.CLB_SerialPortList.SelectedIndexChanged+=CLB_List_SerialPort_SelectedIndexChanged;

            this.CLB_SerialPortList.ItemCheck += CLB_SerialPortList_ItemCheck;

            btn_SPTestMessageSent.Click += btn_SPTestMessageSent_Click;
        }

        /// <summary>
        /// 串口测试按钮点击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void btn_SPTestMessageSent_Click(object sender, EventArgs e)
        {
            int r_code;
            string r_message;
            object r_data;

            if (string.IsNullOrWhiteSpace(comB_SPTest.Text))
            {
                this.MBOX("请选择需测试串口{0}{1}{2}","aa","bb","cc");
                return;
            }

            SerialPort sp;

            if (!Dic_SerialPort.TryGetValue("comB_SPTest.Text", out sp))
            {
                this.MBOX("未找到当前串口,请重启程序");
                return;
            }



            spc.WriteMessageSerialPort(sp, txt_SPTextMessage.Text, out r_code, out r_message, out r_data);
          //  Dic_SerialPort[comB_SPTest.Text].Write(txt_SPTextMessage.Text);

            if (r_code!=0)
            {
                this.MBOX("请选择需测试串口");
            }


             
        }

        /// <summary>
        /// 串口勾选事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CLB_SerialPortList_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            int r_code;
            string r_message;
            object r_data;
            
            if (CLB_SerialPortList.GetItemCheckState(e.Index) == CheckState.Unchecked)//判断所选项的值是否为已勾选
            {
                SerialPort sp;
                if (!Dic_SerialPort.TryGetValue(CLB_SerialPortList.Text.Trim(),out sp))
                {
                    
                }

                spc.OpenSerialPort(Dic_SerialPort[CLB_SerialPortList.Text.Trim()], out r_code, out r_message, out r_data);

                if (r_code!=0)
                {
                    CLB_SerialPortList.SetItemCheckState(e.Index, CheckState.Unchecked);
                    this.MBOX(string.Format("打开{0}串口失败：{1}", CLB_SerialPortList.Text,r_message));
                }
                else
                {
                    CLB_SerialPortList.SetItemCheckState(e.Index, CheckState.Checked);
                    this.MBOX(string.Format("打开{0}串口成功", CLB_SerialPortList.Text));
                }
               
            }
            else
            {
                spc.CloseSerialPort(Dic_SerialPort[CLB_SerialPortList.Text.Trim()], out r_code, out r_message, out r_data);
                if (r_code != 0)
                {
                    this.MBOX(string.Format("打开{0}串口失败：{1}", CLB_SerialPortList.Text, r_message));
                }
                else
                {
                    this.MBOX(string.Format("打开{0}串口成功", CLB_SerialPortList.Text));
                }


                 
            }
         
        }


        

       /// <summary>
       /// 在串口列表中选择串口
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        //private void CLB_List_SerialPort_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //    foreach (KeyValuePair<string,SerialPort> sp in Dic_SerialPort)
        //    {
        //        if (sp.Key==CLB_SerialPortList.Text)
        //        {//循环找到选择的串口的串口对象
        //            f_SPDataAlter f_spda = new f_SPDataAlter(sp.Value);
        //            f_spda.Shown += f_spda_Shown;
        //            f_spda.FormClosing += f_spda_FormClosing;
        //            f_spda.Show();
        //            //打开串口修改对象
        //        }
        //    }
        //}

        /// <summary>
        /// 修改窗口关闭前
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>    
        void f_spda_FormClosing(object sender, FormClosingEventArgs e)
        {
            //关闭修改从窗口后恢复对主窗口的操作权限
            this.Enabled = true;
        }

        /// <summary>
        /// 修改窗口打开前
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void f_spda_Shown(object sender, EventArgs e)
        {
            //开启修改从窗口后关闭对主窗口的操作权限
            this.Enabled = false;
        }

       


        /// <summary>
        /// 串口字典数据刷新
        /// </summary>
        private void Dic_SerialPortRefresh()
        {
            int r_code = -1;
            string r_message = null;
            object data = null;

            //清空串口列表中的内容
            CLB_SerialPortList.Items.Clear();
            comB_SPTest.Items.Clear();
            Dic_SerialPort = new Dictionary<string, SerialPort>();

            string[] PortNameList = SerialPort.GetPortNames();

            foreach (string  pn in PortNameList)
            {

                //创建串口对象
                spc.CreateSerialPortObject(pn, out r_code, out r_message, out data);

                if (r_code != 0)
                {
                    this.MBOX(r_message);
                    return;
                }

                //在串口列表添加该串口
                CLB_SerialPortList.Items.Add(pn);

                //为串口连接测试添加该串口
                comB_SPTest.Items.Add(pn);

                //串口字典添加串口对象
                Dic_SerialPort.Add(pn,(SerialPort)data);
            }
          
        }


        /// <summary>
        /// 连接/断开PLC
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void btn_ComPLC_Click(object sender, EventArgs e)
        {
            int r_code = -1;
            string r_message = null;
            object r_data = null;

            if (ComponentConnStatus.PLC==false)
            {

                if (!int.TryParse(txt_PLCWorkNumber.Text, out PLCWorkNumber))
                {
                    MBOX("上位机工位号错误 请重新输入");
                    return;
                }

                //当前PLC未连接，点击开始连接PLC
                plccton.ConnPLC(PLCWorkNumber,this.axActMLUtlType1,out r_code,out r_message,out r_data);


                if (r_code != 0)
                {
                    MBOX(r_message);
                    return;
                }
                else
                {
                    MBOX("上位机连接成功");

                }

                txt_PLCWorkNumber.Enabled = false;
                b_PLCConState = true;
                btn_ComPLC.Text = "断开上位机";

            }
            else
            {
                if (!int.TryParse(txt_PLCWorkNumber.Text, out PLCWorkNumber))
                {
                    MBOX("上位机工位号错误 请重新输入");
                    return;
                }

                
                //当前PLC已连接，点击开始断开PLC
                plccton.ClosePLC (PLCWorkNumber, this.axActMLUtlType1, out r_code, out r_message, out r_data);


                if (r_code != 0)
                {
                    MBOX(r_message);
                    return;
                }
                else
                {
                    MBOX("上位机断开成功");
                }

                txt_PLCWorkNumber.Enabled = false;
                b_PLCConState = false;
                btn_ComPLC.Text = "连接上位机";
            }
        }


        

        /// <summary>
        /// 用户提示
        /// </summary>
        /// <param name="Message"></param>
        void MBOX( string str,params string [] Message) 
        {

            string message = string.Format(str, Message);
            MessageBox.Show(message);
        }

    }
}
