﻿namespace UpperComputer
{
    partial class f_Home
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(f_Home));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txt_MessageShow = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.comB_SocketList = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btn_SPTestMessageSent = new System.Windows.Forms.Button();
            this.txt_SPTextMessage = new System.Windows.Forms.TextBox();
            this.comB_SPTest = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_Conn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CLB_SerialPortList = new System.Windows.Forms.CheckedListBox();
            this.axActMLUtlType1 = new AxActUtlTypeLib.AxActMLUtlType();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_ComPLC = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_PLCWorkNumber = new System.Windows.Forms.TextBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.axActMLUtlType1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(13, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1251, 670);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txt_MessageShow);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.axActMLUtlType1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1243, 644);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // txt_MessageShow
            // 
            this.txt_MessageShow.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_MessageShow.Location = new System.Drawing.Point(783, 45);
            this.txt_MessageShow.Multiline = true;
            this.txt_MessageShow.Name = "txt_MessageShow";
            this.txt_MessageShow.Size = new System.Drawing.Size(476, 580);
            this.txt_MessageShow.TabIndex = 11;
            this.txt_MessageShow.Text = "  ";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.button2);
            this.groupBox5.Controls.Add(this.textBox4);
            this.groupBox5.Controls.Add(this.comB_SocketList);
            this.groupBox5.Location = new System.Drawing.Point(307, 326);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(443, 186);
            this.groupBox5.TabIndex = 10;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "网口连接测试";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(22, 132);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(151, 44);
            this.button2.TabIndex = 9;
            this.button2.Text = "网口数据发送";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.textBox4.Location = new System.Drawing.Point(22, 82);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(402, 26);
            this.textBox4.TabIndex = 8;
            // 
            // comB_SocketList
            // 
            this.comB_SocketList.FormattingEnabled = true;
            this.comB_SocketList.Location = new System.Drawing.Point(20, 39);
            this.comB_SocketList.Name = "comB_SocketList";
            this.comB_SocketList.Size = new System.Drawing.Size(170, 20);
            this.comB_SocketList.TabIndex = 5;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.btn_SPTestMessageSent);
            this.groupBox4.Controls.Add(this.txt_SPTextMessage);
            this.groupBox4.Controls.Add(this.comB_SPTest);
            this.groupBox4.Location = new System.Drawing.Point(307, 125);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(443, 177);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "串口连接测试";
            // 
            // btn_SPTestMessageSent
            // 
            this.btn_SPTestMessageSent.Location = new System.Drawing.Point(22, 115);
            this.btn_SPTestMessageSent.Name = "btn_SPTestMessageSent";
            this.btn_SPTestMessageSent.Size = new System.Drawing.Size(151, 44);
            this.btn_SPTestMessageSent.TabIndex = 7;
            this.btn_SPTestMessageSent.Text = "串口数据发送";
            this.btn_SPTestMessageSent.UseVisualStyleBackColor = true;
            // 
            // txt_SPTextMessage
            // 
            this.txt_SPTextMessage.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.txt_SPTextMessage.Location = new System.Drawing.Point(22, 65);
            this.txt_SPTextMessage.Name = "txt_SPTextMessage";
            this.txt_SPTextMessage.Size = new System.Drawing.Size(402, 26);
            this.txt_SPTextMessage.TabIndex = 6;
            // 
            // comB_SPTest
            // 
            this.comB_SPTest.FormattingEnabled = true;
            this.comB_SPTest.Location = new System.Drawing.Point(22, 27);
            this.comB_SPTest.Name = "comB_SPTest";
            this.comB_SPTest.Size = new System.Drawing.Size(168, 20);
            this.comB_SPTest.TabIndex = 5;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.btn_Conn);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Controls.Add(this.textBox2);
            this.groupBox3.Location = new System.Drawing.Point(24, 439);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(242, 186);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "网口链接";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "端口";
            // 
            // btn_Conn
            // 
            this.btn_Conn.Location = new System.Drawing.Point(21, 138);
            this.btn_Conn.Name = "btn_Conn";
            this.btn_Conn.Size = new System.Drawing.Size(76, 23);
            this.btn_Conn.TabIndex = 7;
            this.btn_Conn.Text = "链接网口";
            this.btn_Conn.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 12);
            this.label2.TabIndex = 6;
            this.label2.Text = "网址";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(19, 58);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(197, 21);
            this.textBox1.TabIndex = 4;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(21, 97);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(121, 21);
            this.textBox2.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CLB_SerialPortList);
            this.groupBox2.Location = new System.Drawing.Point(24, 229);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(242, 186);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "串口连接";
            // 
            // CLB_SerialPortList
            // 
            this.CLB_SerialPortList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.CLB_SerialPortList.FormattingEnabled = true;
            this.CLB_SerialPortList.Location = new System.Drawing.Point(19, 36);
            this.CLB_SerialPortList.Name = "CLB_SerialPortList";
            this.CLB_SerialPortList.Size = new System.Drawing.Size(197, 132);
            this.CLB_SerialPortList.TabIndex = 3;
            // 
            // axActMLUtlType1
            // 
            this.axActMLUtlType1.Enabled = true;
            this.axActMLUtlType1.Location = new System.Drawing.Point(15, 3);
            this.axActMLUtlType1.Name = "axActMLUtlType1";
            this.axActMLUtlType1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axActMLUtlType1.OcxState")));
            this.axActMLUtlType1.Size = new System.Drawing.Size(32, 32);
            this.axActMLUtlType1.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_ComPLC);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txt_PLCWorkNumber);
            this.groupBox1.Location = new System.Drawing.Point(24, 31);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(242, 175);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PLC连接";
            // 
            // btn_ComPLC
            // 
            this.btn_ComPLC.Location = new System.Drawing.Point(28, 94);
            this.btn_ComPLC.Name = "btn_ComPLC";
            this.btn_ComPLC.Size = new System.Drawing.Size(144, 38);
            this.btn_ComPLC.TabIndex = 2;
            this.btn_ComPLC.Text = "连接PLC";
            this.btn_ComPLC.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "PLC工位号：";
            // 
            // txt_PLCWorkNumber
            // 
            this.txt_PLCWorkNumber.Location = new System.Drawing.Point(101, 34);
            this.txt_PLCWorkNumber.Name = "txt_PLCWorkNumber";
            this.txt_PLCWorkNumber.Size = new System.Drawing.Size(71, 21);
            this.txt_PLCWorkNumber.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1243, 644);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // f_Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1276, 694);
            this.Controls.Add(this.tabControl1);
            this.Name = "f_Home";
            this.Text = "Form1";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.axActMLUtlType1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txt_PLCWorkNumber;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_ComPLC;
        private System.Windows.Forms.Label label1;
        private AxActUtlTypeLib.AxActMLUtlType axActMLUtlType1;
        private System.Windows.Forms.CheckedListBox CLB_SerialPortList;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox comB_SocketList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_Conn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comB_SPTest;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button btn_SPTestMessageSent;
        private System.Windows.Forms.TextBox txt_SPTextMessage;
        private System.Windows.Forms.TextBox txt_MessageShow;

    }
}

