﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UpperComputer
{
    public partial class f_SPDataAlter : Form
    {
        public SerialPort sp;
        public f_SPDataAlter(SerialPort a)
        {
            InitializeComponent();
            sp = a;//获得一个串口对象

        }

        private void form_SPDataAlter_Load(object sender, EventArgs e)
        {

            cmb_StopBits.DataSource = new string[] { "One", "OnePointFive", "Two", "None" };
            cmb_StopBits.DropDownStyle = ComboBoxStyle.DropDownList;

            cmb_Parity.DataSource = new string[] { "Odd", "Even", "None" };
            cmb_Parity.DropDownStyle = ComboBoxStyle.DropDownList;



            lbl_SPName.Text = sp.PortName;
            txt_BaudRate.Text = sp.BaudRate.ToString();
            txt_DataBits.Text = sp.DataBits.ToString();

            if (sp.StopBits == StopBits.None)
            {
                this.cmb_StopBits.Text = "";
            }
            else
            {
                this.cmb_StopBits.Text = sp.StopBits.ToString();
            }

            if (sp.Parity == Parity.None)
            {
                this.cmb_Parity.Text = "";
            }
            else
            {
                this.cmb_Parity.Text = sp.Parity.ToString();
            }

            btn_Alter.Click += btn_Alter_Click;

        }



        void btn_Alter_Click(object sender, EventArgs e)
        {
            int i_BaudRate;
            if (!int.TryParse(txt_BaudRate.Text, out i_BaudRate))
            {
                MessageBox.Show("Error：波特率不正确!", "Error");
                return;
            }
            sp.BaudRate = i_BaudRate;


            int i_DataBits;
            if (!int.TryParse(txt_DataBits.Text, out i_DataBits))
            {
                MessageBox.Show("Error：数据位不正确!", "Error");
                return;
            }
            sp.DataBits = i_DataBits;

            switch (cmb_StopBits.Text)
            {
                case "One":
                    sp.StopBits = System.IO.Ports.StopBits.One;
                    break;
                case "OnePointFive":
                    sp.StopBits = System.IO.Ports.StopBits.OnePointFive;
                    break;
                case "Two":
                    sp.StopBits = System.IO.Ports.StopBits.Two;
                    break;
                default:
                    sp.StopBits = System.IO.Ports.StopBits.None;
                    break;

            }


            switch (cmb_Parity.Text)
            {
                case "Odd":
                    sp.Parity = Parity.Odd;
                    break;
                case "Even":
                    sp.Parity = Parity.Even;
                    break;
                default:
                    sp.Parity = Parity.None;
                    break;
            }


            this.Close();

        }
    }
}
