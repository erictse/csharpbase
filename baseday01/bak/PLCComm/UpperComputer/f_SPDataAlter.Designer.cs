﻿namespace UpperComputer
{
    partial class f_SPDataAlter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_CheckBit = new System.Windows.Forms.Label();
            this.lbl_StopBits = new System.Windows.Forms.Label();
            this.lbl_DatsBits = new System.Windows.Forms.Label();
            this.lbl_BaudRate = new System.Windows.Forms.Label();
            this.txt_BaudRate = new System.Windows.Forms.TextBox();
            this.txt_DataBits = new System.Windows.Forms.TextBox();
            this.btn_Alter = new System.Windows.Forms.Button();
            this.cmb_StopBits = new System.Windows.Forms.ComboBox();
            this.cmb_Parity = new System.Windows.Forms.ComboBox();
            this.lbl_SPName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbl_CheckBit
            // 
            this.lbl_CheckBit.AutoSize = true;
            this.lbl_CheckBit.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold);
            this.lbl_CheckBit.Location = new System.Drawing.Point(38, 217);
            this.lbl_CheckBit.Name = "lbl_CheckBit";
            this.lbl_CheckBit.Size = new System.Drawing.Size(80, 19);
            this.lbl_CheckBit.TabIndex = 11;
            this.lbl_CheckBit.Text = "检查位:";
            // 
            // lbl_StopBits
            // 
            this.lbl_StopBits.AutoSize = true;
            this.lbl_StopBits.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold);
            this.lbl_StopBits.Location = new System.Drawing.Point(38, 173);
            this.lbl_StopBits.Name = "lbl_StopBits";
            this.lbl_StopBits.Size = new System.Drawing.Size(80, 19);
            this.lbl_StopBits.TabIndex = 10;
            this.lbl_StopBits.Text = "停止位:";
            // 
            // lbl_DatsBits
            // 
            this.lbl_DatsBits.AutoSize = true;
            this.lbl_DatsBits.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold);
            this.lbl_DatsBits.Location = new System.Drawing.Point(38, 129);
            this.lbl_DatsBits.Name = "lbl_DatsBits";
            this.lbl_DatsBits.Size = new System.Drawing.Size(80, 19);
            this.lbl_DatsBits.TabIndex = 9;
            this.lbl_DatsBits.Text = "数据位:";
            // 
            // lbl_BaudRate
            // 
            this.lbl_BaudRate.AutoSize = true;
            this.lbl_BaudRate.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_BaudRate.Location = new System.Drawing.Point(38, 90);
            this.lbl_BaudRate.Name = "lbl_BaudRate";
            this.lbl_BaudRate.Size = new System.Drawing.Size(80, 19);
            this.lbl_BaudRate.TabIndex = 8;
            this.lbl_BaudRate.Text = "波特率:";
            // 
            // txt_BaudRate
            // 
            this.txt_BaudRate.Location = new System.Drawing.Point(135, 88);
            this.txt_BaudRate.Name = "txt_BaudRate";
            this.txt_BaudRate.Size = new System.Drawing.Size(195, 21);
            this.txt_BaudRate.TabIndex = 12;
            // 
            // txt_DataBits
            // 
            this.txt_DataBits.Location = new System.Drawing.Point(135, 127);
            this.txt_DataBits.Name = "txt_DataBits";
            this.txt_DataBits.Size = new System.Drawing.Size(195, 21);
            this.txt_DataBits.TabIndex = 13;
            // 
            // btn_Alter
            // 
            this.btn_Alter.Font = new System.Drawing.Font("宋体", 14.25F, System.Drawing.FontStyle.Bold);
            this.btn_Alter.Location = new System.Drawing.Point(97, 262);
            this.btn_Alter.Name = "btn_Alter";
            this.btn_Alter.Size = new System.Drawing.Size(159, 33);
            this.btn_Alter.TabIndex = 16;
            this.btn_Alter.Text = "确定修改";
            this.btn_Alter.UseVisualStyleBackColor = true;
            // 
            // cmb_StopBits
            // 
            this.cmb_StopBits.FormattingEnabled = true;
            this.cmb_StopBits.Location = new System.Drawing.Point(135, 173);
            this.cmb_StopBits.Name = "cmb_StopBits";
            this.cmb_StopBits.Size = new System.Drawing.Size(195, 20);
            this.cmb_StopBits.TabIndex = 17;
            // 
            // cmb_Parity
            // 
            this.cmb_Parity.FormattingEnabled = true;
            this.cmb_Parity.Location = new System.Drawing.Point(135, 215);
            this.cmb_Parity.Name = "cmb_Parity";
            this.cmb_Parity.Size = new System.Drawing.Size(195, 20);
            this.cmb_Parity.TabIndex = 18;
            // 
            // lbl_SPName
            // 
            this.lbl_SPName.AutoSize = true;
            this.lbl_SPName.Font = new System.Drawing.Font("宋体", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbl_SPName.Location = new System.Drawing.Point(100, 21);
            this.lbl_SPName.Name = "lbl_SPName";
            this.lbl_SPName.Size = new System.Drawing.Size(147, 33);
            this.lbl_SPName.TabIndex = 19;
            this.lbl_SPName.Text = "串口名称";
            // 
            // form_SPDataAlter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 312);
            this.Controls.Add(this.lbl_SPName);
            this.Controls.Add(this.cmb_Parity);
            this.Controls.Add(this.cmb_StopBits);
            this.Controls.Add(this.btn_Alter);
            this.Controls.Add(this.txt_DataBits);
            this.Controls.Add(this.txt_BaudRate);
            this.Controls.Add(this.lbl_CheckBit);
            this.Controls.Add(this.lbl_StopBits);
            this.Controls.Add(this.lbl_DatsBits);
            this.Controls.Add(this.lbl_BaudRate);
            this.Name = "form_SPDataAlter";
            this.Text = "接口参数修改";
            this.Load += new System.EventHandler(this.form_SPDataAlter_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_CheckBit;
        private System.Windows.Forms.Label lbl_StopBits;
        private System.Windows.Forms.Label lbl_DatsBits;
        private System.Windows.Forms.Label lbl_BaudRate;
        private System.Windows.Forms.TextBox txt_BaudRate;
        private System.Windows.Forms.TextBox txt_DataBits;
        private System.Windows.Forms.Button btn_Alter;
        private System.Windows.Forms.ComboBox cmb_StopBits;
        private System.Windows.Forms.ComboBox cmb_Parity;
        private System.Windows.Forms.Label lbl_SPName;

    }
}