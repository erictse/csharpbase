﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO.Ports;
using Newtonsoft.Json;
using System.Configuration;

namespace UpperComputer
{
    class SerialPortControl
    {

        /// <summary>
        /// 输入接口名称（COM1）创建一个串口对象
        /// </summary>
        /// <param name="SPName"></param>
        /// <param name="r_code"></param>
        /// <param name="r_message"></param>
        /// <param name="SPObjert"></param>
        public void CreateSerialPortObject(string SPName, out int r_code, out string r_message, out object SPObjert)
        {
            SerialPort sp=new SerialPort ();
            sp.PortName=SPName;


           // JsonConvert.SerializeObject(sp);



            //在配置表查找该名称的接口Json字符串
            string SPjson=ConfigurationManager.AppSettings[SPName] ;

          
            //判断Json字符串是否为空
            if (!string.IsNullOrWhiteSpace(SPjson))
            {
                sp = JsonConvert.DeserializeObject<SerialPort>(SPjson);
            }
     

            r_code = 0;
            r_message = "生成成功";
            SPObjert = sp;

            
           
        }

        /// <summary>
        /// 开启串口
        /// </summary>
        /// <param name="SP">串口类</param>1`
        /// <param name="r_code"></param>
        /// <param name="r_message"></param>
        /// <param name="r_data"></param>
        public void OpenSerialPort(SerialPort SP, out int r_code, out string r_message, out object r_data) 
        {

            SP.Open();

            if (SP.IsOpen)
            {
                r_code = 0;
                r_message = "已开启";
                r_data = null;
            }
            else
            {
                r_code = 1;
                r_message = "串口开启失败";
                r_data = null;
            }

        }


          public void CloseSerialPort(SerialPort SP, out int r_code, out string r_message, out object r_data) 
        {

            SP.Close();

            if (!SP.IsOpen)
            {
                r_code = 0;
                r_message = "已关闭";
                r_data = null;
            }
            else
            {
                r_code = 1;
                r_message = "串口关闭失败";
                r_data = null;
            }

        }


          internal void WriteMessageSerialPort(SerialPort SP,string message, out int r_code, out string r_message, out object r_data)
          {
              if (!SP.IsOpen)
              {
                  r_code = 1;
                  r_message = "串口未打开";
                  r_data = null;
                  return;
              }

              SP.WriteLine(message);

              r_code = 0;
              r_message = "发送："+message;
              r_data = null;

          }


    }
}
