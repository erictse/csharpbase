﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using AxActUtlTypeLib;

namespace UpperComputer
{
    class PLCControl
    {


        /// </summary>
        public void ClosePLC(int PLCWorkNumber, AxActMLUtlType axActUtlType, out  int r_code, out string r_message, out object r_data)
        {



            axActUtlType.ActLogicalStationNumber = PLCWorkNumber;
            int commRtn = (int)axActUtlType.Close();
            if (commRtn == 0)
            {
                ComponentConnStatus.PLC = false;
                r_code = 0;
                r_message = "断开成功";
                r_data = null;
            }
            else
            {
                r_code = 1;
                r_message = "断开失败";
                r_data = null;
            }



        }




        public void ConnPLC(int PLCWorkNumber, AxActMLUtlType axActUtlType, out int r_code, out string r_message, out object r_data)
        {
            axActUtlType.ActLogicalStationNumber = PLCWorkNumber;
 
            int commRtn = (int)axActUtlType.Open();
            if (commRtn == 0)
            {
                ComponentConnStatus.PLC = true;
                r_code = 0;
                r_message = "连接成功";
                r_data = null;
            }
            else
            {
                r_code = 1;
                r_message = "连接失败";
                r_data = null;


            }
        }
    }
}