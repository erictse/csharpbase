﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PLCComm
{
    public partial class form_inter : Form
    {
        public form_inter()
        {
            InitializeComponent();
            FormLoad();
        }

        private void FormLoad()
        {
            this.btn_CommPLC.Click += btn_CommPLC_Click;
            this.btn_getPLCdata.Click += btn_getPLCdata_Click;
            this.btn_SetPLCData.Click += btn_SetPLCData_Click;
            this.button1.Click += button1_Click;
            this.button2.Click += button2_Click;
        }

        void button2_Click(object sender, EventArgs e)
        {
            string addr = "D68";
            int num = 1;
            short[] data = new short[num];
            try
            {
                //ReadDeviceRandom2
                int dataRtn = this.axActUtlType1.GetDevice2(addr, out data[0]);
                if (dataRtn == 0)
                {
                    //Console.WriteLine(string.Format("读取数据成功，{0：X4}",data[0]));
                    MessageBox.Show(string.Format("读取数据成功，{0}", data[0]));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        void button1_Click(object sender, EventArgs e)
        {
            string addr = "D68";
            int num = 1;
            short[] data = new short[num];

            data[0] = 123;
            try
            {
                int dataRtn = this.axActUtlType1.SetDevice(addr, 99999);
                if (dataRtn == 0)
                {
                    Console.WriteLine("数据写入成功");
                    MessageBox.Show("数据写入成功");
                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.ToString());
            }
        }

        void btn_SetPLCData_Click(object sender, EventArgs e)
        {
            string addr = "M170";
            int num = 1;
            short[] data = new short[num];

            data[0] = 9999;
            try
            {
                int dataRtn = this.axActUtlType1.WriteDeviceBlock2(addr, num, ref data[0]);
                if (dataRtn==0)
                {
                    Console.WriteLine("数据写入成功");
                    MessageBox.Show("数据写入成功");
                }
 
            }
            catch (Exception  ex)
            {

                MessageBox.Show(ex.ToString());
            }
        }

        void btn_getPLCdata_Click(object sender, EventArgs e)
        {
            string addr = "M170";
            int num = 1;
            short[] data = new short[num];
            try
            {
                //ReadDeviceRandom2
                int dataRtn = this.axActUtlType1.ReadDeviceBlock2(addr, num, out data[0]);
                if (dataRtn==0)
                {
                    //Console.WriteLine(string.Format("读取数据成功，{0：X4}",data[0]));
                    MessageBox.Show(string.Format("读取数据成功，{0}",data[0]));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        void btn_CommPLC_Click(object sender, EventArgs e)
        {
            try
            {
                int commRtn = this.axActUtlType1.Open();
                if (commRtn==0)
                {
                    MessageBox.Show(string.Format("连接成功"));
                }

            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.ToString());
            }
        }
    }
}
