﻿namespace PLCComm
{
    partial class form_inter
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(form_inter));
            this.axActUtlType1 = new AxActUtlTypeLib.AxActUtlType();
            this.btn_CommPLC = new System.Windows.Forms.Button();
            this.btn_getPLCdata = new System.Windows.Forms.Button();
            this.btn_SetPLCData = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.axActUtlType1)).BeginInit();
            this.SuspendLayout();
            // 
            // axActUtlType1
            // 
            this.axActUtlType1.Enabled = true;
            this.axActUtlType1.Location = new System.Drawing.Point(35, 25);
            this.axActUtlType1.Name = "axActUtlType1";
            this.axActUtlType1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axActUtlType1.OcxState")));
            this.axActUtlType1.Size = new System.Drawing.Size(32, 32);
            this.axActUtlType1.TabIndex = 0;
            // 
            // btn_CommPLC
            // 
            this.btn_CommPLC.Location = new System.Drawing.Point(35, 79);
            this.btn_CommPLC.Name = "btn_CommPLC";
            this.btn_CommPLC.Size = new System.Drawing.Size(75, 23);
            this.btn_CommPLC.TabIndex = 1;
            this.btn_CommPLC.Text = "连接PLC";
            this.btn_CommPLC.UseVisualStyleBackColor = true;
            // 
            // btn_getPLCdata
            // 
            this.btn_getPLCdata.Location = new System.Drawing.Point(35, 198);
            this.btn_getPLCdata.Name = "btn_getPLCdata";
            this.btn_getPLCdata.Size = new System.Drawing.Size(115, 23);
            this.btn_getPLCdata.TabIndex = 2;
            this.btn_getPLCdata.Text = "获取PLC数据";
            this.btn_getPLCdata.UseVisualStyleBackColor = true;
            // 
            // btn_SetPLCData
            // 
            this.btn_SetPLCData.Location = new System.Drawing.Point(184, 106);
            this.btn_SetPLCData.Name = "btn_SetPLCData";
            this.btn_SetPLCData.Size = new System.Drawing.Size(115, 23);
            this.btn_SetPLCData.TabIndex = 3;
            this.btn_SetPLCData.Text = "写入PLC数据";
            this.btn_SetPLCData.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(35, 140);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(128, 21);
            this.textBox1.TabIndex = 4;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(184, 150);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(115, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "写入PLC数据2";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(35, 227);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(115, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "获取PLC数据2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // form_inter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 269);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btn_SetPLCData);
            this.Controls.Add(this.btn_getPLCdata);
            this.Controls.Add(this.btn_CommPLC);
            this.Controls.Add(this.axActUtlType1);
            this.Name = "form_inter";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.axActUtlType1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private AxActUtlTypeLib.AxActUtlType axActUtlType1;
        private System.Windows.Forms.Button btn_CommPLC;
        private System.Windows.Forms.Button btn_getPLCdata;
        private System.Windows.Forms.Button btn_SetPLCData;
        private System.Windows.Forms.TextBox textBox1;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

